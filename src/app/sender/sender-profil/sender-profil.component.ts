import { Component, OnInit } from '@angular/core';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { Sender, Times } from 'src/app/_models/sender';
import { Product } from 'src/app/_models/product';
import { isUndefined } from 'util';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';

@Component({
  selector: 'app-sender-profil',
  templateUrl: './sender-profil.component.html',
  styleUrls: ['./sender-profil.component.scss']
})
export class SenderProfilComponent implements OnInit {
  sender:Sender = new Sender();
  selectedProduct : Product = new Product();
  updateBanner:boolean=false;
  banner:any="";
  selectedFiles: FileList;
  newProduct=true;
  Timeframe;
  DaysOfTheWeek = ["Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi"];
  today = new Date();
  constructor(private senderService:SenderService,private loginService:SenderLoginService) { }

  ngOnInit() {
    this.getSender();
  }



 getTime(){
  console.log(this.sender.times)
   if(this.sender.times.length === 0){
    
    this.DaysOfTheWeek.forEach((value,index) => {
      this.sender.times[index]= new Times(value,index)
    });
    
   }
 }
  getSender(){
    this.senderService.getSender(this.loginService.getSender()).subscribe(sender=>{
      this.sender = sender;
      console.log(this.sender.menu)
      this.getTime()
    });
  }

  close(){
    this.banner="";
    this.updateBanner=false;
    this.initProduct();
  }
  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.banner=this.selectedFiles.item(0).name
  }
  update_Banner(){
 this.senderService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
   this.sender.banner = data 
   this.update();
 })
  }
  addProduct(form){
    if(this.selectFile){
      this.senderService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
        this.selectedProduct.image = data 
        let id =(this.sender.menu)?this.sender.menu.length:0;
        this.sender.menu=(this.sender.menu)?this.sender.menu:[];
        this.sender.menu[id] = this.selectedProduct;
     form.hide();
     this.update();
     this.close()

      })
    }
   
  }
  updateProduct(form){
    if(this.selectFile){
      this.senderService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
        this.selectedProduct.image = data 
        this.sender.menu[this.selectedProduct._id] = this.selectedProduct;
     form.hide();
     this.update();
     this.close()
      })
    }
   
  }
  update(){
   this.senderService.updateSender(this.sender).subscribe(sender=>{console.log(sender) ;this.sender = sender
    this.selectedProduct =new Product()})
   
  }
  initProduct(){
    this.selectedProduct =new Product()
  }
  delete(id){
    this.sender.menu.splice(id,1)
    this.update();
  }

  setTime(){

this.senderService.updateSender(this.sender).subscribe(sender=>{console.log(sender)})

  }

  setminOrder(){
    this.senderService.updateSender(this.sender).subscribe(sender=>{console.log(sender)})
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../_guards/auth-guard.service';
import { SenderComponent } from './sender.component';
import { SenderProfilComponent } from './sender-profil/sender-profil.component';
import { SenderSigninComponent } from './sender-signin/sender-signin.component';
import { SenderDeliveryWaitListComponent } from './sender-delivery-wait-list/sender-delivery-wait-list.component';
import { SenderDeliveryHistoryComponent } from './sender-delivery-history/sender-delivery-history.component';
 import {DeliveriesInProgressComponent} from'./deliveries-in-progress/deliveries-in-progress.component';
 import { MailConfirmationComponent } from './mail-confirmation/mail-confirmation.component'
 import { SenderNewProductComponent } from './sender-new-product/sender-new-product.component'
import { SenderUpdateProductComponent } from './sender-update-product/sender-update-product.component';


const routes: Routes = [
  {
    path: '',
    component: SenderComponent,
    canActivate: [AuthGuardService],
    children: [
        {
            path: 'restaurant/account',
            component: SenderProfilComponent,
            canActivate: [AuthGuardService]
        },
        {
          path: 'restaurant/waitList',
          component: SenderDeliveryWaitListComponent,
          canActivate: [AuthGuardService]
      },

      {
        path: 'restaurant/history',
        component: SenderDeliveryHistoryComponent,
        canActivate: [AuthGuardService]
    },
    {
      path: 'restaurant/inprogress',
      component: DeliveriesInProgressComponent,
      canActivate: [AuthGuardService]
  },
  {
    path: 'restaurant/newproduct',
    component : SenderNewProductComponent,
    canActivate:[AuthGuardService]
  },
    {
      path:'restaurant/account/updateproduct/:id',
      component: SenderUpdateProductComponent,
      canActivate:[AuthGuardService]
    }  

        
    ]
},
{
  path: 'restaurant',
  component: SenderSigninComponent,
},
{
  path: 'restaurant/confirmation/:token/:mail',
  component: MailConfirmationComponent,
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SenderRoutingModule { }

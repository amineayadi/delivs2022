import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderNewProductComponent } from './sender-new-product.component';

describe('SenderNewProductComponent', () => {
  let component: SenderNewProductComponent;
  let fixture: ComponentFixture<SenderNewProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderNewProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderNewProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

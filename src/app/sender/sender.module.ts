import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- #1 import module
import { SenderRoutingModule } from './sender-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SenderHeaderComponent } from './sender-header/sender-header.component';
import { SenderFooterComponent } from './sender-footer/sender-footer.component';
import { SenderProfilComponent } from './sender-profil/sender-profil.component';
import { SenderSigninComponent } from './sender-signin/sender-signin.component';
import { SenderDeliveryWaitListComponent } from './sender-delivery-wait-list/sender-delivery-wait-list.component';
import { DeliveriesInProgressComponent } from './deliveries-in-progress/deliveries-in-progress.component';
import { SenderDeliveryHistoryComponent } from './sender-delivery-history/sender-delivery-history.component';
import { GoogleModule} from './../google/google.module';
import { MailConfirmationComponent } from './mail-confirmation/mail-confirmation.component';
import { SenderNewProductComponent } from './sender-new-product/sender-new-product.component';
import { SenderUpdateProductComponent } from './sender-update-product/sender-update-product.component'

@NgModule({
  declarations: [ SenderHeaderComponent, SenderFooterComponent,SenderProfilComponent, SenderSigninComponent,
      SenderDeliveryWaitListComponent,
       DeliveriesInProgressComponent, SenderDeliveryHistoryComponent, MailConfirmationComponent, SenderNewProductComponent, SenderUpdateProductComponent ],
  imports: [
    GoogleModule,
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule, 
    SenderRoutingModule,
    MDBBootstrapModule.forRoot(),
  ],
  exports:[ SenderHeaderComponent,SenderFooterComponent]
})
export class SenderModule { }

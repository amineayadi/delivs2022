import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveriesInProgressComponent } from './deliveries-in-progress.component';
 
describe('DeliveriesInProgressComponent', () => {
  let component: DeliveriesInProgressComponent;
  let fixture: ComponentFixture<DeliveriesInProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveriesInProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveriesInProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

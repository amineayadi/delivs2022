import { Component, OnInit } from '@angular/core';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { Sender, Times } from 'src/app/_models/sender';
import { Product, Option, OptionLine } from 'src/app/_models/product';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/_models/category';
import { CategoryService } from 'src/app/_services/category/category.service';

@Component({
  selector: 'app-sender-update-product',
  templateUrl: './sender-update-product.component.html',
  styleUrls: ['./sender-update-product.component.scss']
})
export class SenderUpdateProductComponent implements OnInit {
  sender:Sender = new Sender();
  selectedProduct : Product = new Product();
  updateBanner:boolean=false;
  option : Option = new Option();
  optionLine: OptionLine= new OptionLine();
  banner:any="";
  selectedFiles: FileList;
  selectedid;
  selectedidOL;
  categories:Category[]= [];
  DaysOfTheWeek = ["Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Tous les jours","Non Disponible"];
  constructor(private route: ActivatedRoute,private senderService:SenderService,private loginService:SenderLoginService, private router : Router
    ,private categoryService:CategoryService) { }

  ngOnInit() {
    this.getSender();
    this.getCategories();
  }
  getCategories(){
    this.categoryService.getAllcategories().subscribe(categories=>{
      this.categories=categories;
    });
  }
  readUrlParams(callback) {
    this.route.queryParams.subscribe(queryParams => {
      this.route.params.subscribe(routeParams => {
        callback(routeParams, queryParams);
      });
    });
  }
  getSender() {
    this.readUrlParams((routeParams, queryParams) => {
      let id = routeParams.id;
      this.senderService.getSender(this.loginService.getSender()).subscribe(sender=>{this.sender = sender;
        this.selectedProduct=this.sender.menu[id]; console.log(this.selectedProduct) });
     

  
    });
   
  }
  initOptionLine(option:Option){
    option.optionLines[0] = new OptionLine();
  }
  addOption(){
    let id =this.selectedProduct.options.length;
    this.selectedProduct.options[id] = new Option();
    this.initOptionLine(this.selectedProduct.options[id])
  }
  addOptionLine(idO){
    let id =this.selectedProduct.options[idO].optionLines.length;
    this.selectedProduct.options[idO].optionLines[id] = new OptionLine();
  }
  update(){
    this.senderService.updateSender(this.sender).subscribe(sender=>{console.log(sender) ;this.sender = sender
      this.router.navigate(["restaurant/account"])})
    
   }
   updateProduct(){
    if(this.updateBanner){
      this.senderService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
        this.selectedProduct.image = data 
        this.sender.menu[this.selectedProduct._id] = this.selectedProduct;
    
     this.update();
   
      })
    }
    else {
      this.sender.menu[this.selectedProduct._id] = this.selectedProduct;
     this.update();
    }
   
  }
  delete(idO){
    this.readUrlParams((routeParams, queryParams) => {
      let id = routeParams.id;
      this.sender.menu[id].options.splice(idO,1)
      this.senderService.updateSender(this.sender).subscribe(sender=>{console.log(sender) ;this.sender = sender
      })
     

  
    });
    
  }
  deleteOptionLine(selectedid, selectedidOL){
    this.readUrlParams((routeParams, queryParams) => {
      let id = routeParams.id;
      this.sender.menu[id].options[selectedid].optionLines.splice(selectedidOL,1);
      this.senderService.updateSender(this.sender).subscribe(sender=>{console.log(sender) ;this.sender = sender
      })
     

  
    });
  }
  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.banner=this.selectedFiles.item(0).name;
    this.updateBanner=true;
  }



}

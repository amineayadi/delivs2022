import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderSigninComponent } from './sender-signin.component';

describe('SenderSigninComponent', () => {
  let component: SenderSigninComponent;
  let fixture: ComponentFixture<SenderSigninComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderSigninComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderSigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { Delivery,history } from 'src/app/_models/delivery';
import { DeliveryService } from 'src/app/_services/delivery/delivery.service';
import { OrderService } from 'src/app/_services/order/order.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import {Notification} from './../../_models/notification';


@Component({
  selector: 'app-sender-delivery-wait-list',
  templateUrl: './sender-delivery-wait-list.component.html',
  styleUrls: ['./sender-delivery-wait-list.component.scss']
})
export class SenderDeliveryWaitListComponent implements OnInit {
@ViewChild('frame', { static: true }) public formModal;
  deliveries:Delivery[]=[];
  selectedId:string="";
  deliveryMen:DeliveryMan[]=[]
  valid=false;
  constructor(private deliveryService:DeliveryService,private orderService:OrderService,private recipientService:RecipientService,
    private delieveryManService:DeliveryManService) { }

  ngOnInit() {
    this.getDeliveryList();
    this.getDeliveryMen();
  }
  getDeliveryList(){
    this.deliveryService.getDeliveryBySenderAndStatus('init,wait').subscribe(deliveries => {
      
      this.deliveries=deliveries;
      this.deliveries.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index)})
      
    })
  }
  getDeliveryMen(){
    this.delieveryManService.getAllDeliveryMans().subscribe(deliveryMen=>{
      this.deliveryMen=deliveryMen;
    })
  }
  getOrder(id){
    let delivery = this.deliveries[id];
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }
    getRecipient(id){
      let delivery = this.deliveries[id];
      if(delivery.recipient){
        this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
          delivery.recipient=  recipient.firstName + " "+ recipient.lastName;
        });
      }  
    }

    affect(id){
      this.deliveryService.getDelivery(this.selectedId).subscribe(delivery=>{
       
        delivery.deliveryMan=id;
        delivery.status="affected";
        this.deliveryService.updateDelivery(delivery).subscribe(delivery=>{
          this.formModal.hide();this.getDeliveryList();
        })
      })
    }
    confirm(id){
      this.deliveryService.getDelivery(id).subscribe(delivery=>{

        delivery.status="wait";
        delivery.statusHistory.push(new history(delivery.status));
        this.deliveryService.updateDelivery(delivery).subscribe(delivery=>{
          let notif = new Notification("Nouvelle livraison","Une nouvelle livraison est arrivée","/waitList","livreurs");
          this.deliveryService.sendNotification(notif).subscribe(data=>{
            this.getDeliveryList();
          })
          let notifClient = new Notification("Nouvelle Commande","Votre commande est confirmée par le restaurant ","/recipient/orders",delivery.recipient);
          this.deliveryService.sendNotification(notifClient).subscribe(data=>{
            this.getDeliveryList();
          }) 
        })
      })
    }
    cancel(id){
      this.deliveryService.getDelivery(id).subscribe(delivery=>{

        delivery.status="refused";
        delivery.statusHistory.push(new history(delivery.status));
        this.deliveryService.updateDelivery(delivery).subscribe(delivery=>{
          let notifClient = new Notification("Nouvelle Commande","Malheuresement, votre commande est refusée par le restaurant ","/recipient/orders",delivery.recipient);
          this.deliveryService.sendNotification(notifClient).subscribe(data=>{
            this.getDeliveryList();
          })
         
        })
      })
    }
     
    
   
  

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderDeliveryWaitListComponent } from './sender-delivery-wait-list.component';

describe('SenderDeliveryWaitListComponent', () => {
  let component: SenderDeliveryWaitListComponent;
  let fixture: ComponentFixture<SenderDeliveryWaitListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderDeliveryWaitListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderDeliveryWaitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

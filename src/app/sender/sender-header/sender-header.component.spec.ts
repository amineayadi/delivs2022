import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderHeaderComponent } from './sender-header.component';

describe('SenderHeaderComponent', () => {
  let component: SenderHeaderComponent;
  let fixture: ComponentFixture<SenderHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

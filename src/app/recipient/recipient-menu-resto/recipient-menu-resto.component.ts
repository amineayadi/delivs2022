import { Component, OnInit, ViewChild, ViewChildren, QueryList, ElementRef } from '@angular/core';
import{ Sender, Times,getNewPrice,getDelivsPrice } from '../../_models/sender';
import { SenderService } from '../../_services/sender/sender.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from '../../_models/order';
import { OrderItem } from '../../_models/orderItem';
import { OrderService } from '../../_services/order/order.service';
import {Location} from '@angular/common';
import { CartService } from 'src/app/_services/cart/cart-service.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Delivery,history } from '../../_models/delivery';
import { DeliveryService } from '../../_services/delivery/delivery.service';
import { RecipientLoginService } from '../../_services/recipient/recipient-login.service';
import { Product } from 'src/app/_models/product';
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';
import { GoogleComponent } from '../../google/google/google.component';
import { DeliveryFee } from 'src/app/_models/deliveryFee';
import * as sha1 from 'sha1';
import {Md5} from 'ts-md5/dist/md5';
import { payment } from 'src/app/_models/payment';
import {Notification} from './../../_models/notification';
import { Category } from '../../_models/category';
import { CategoryService } from 'src/app/_services/category/category.service';
import { NoteService } from 'src/app/_services/note/NoteService';
import { readmore } from './../../_services/truncate';
import { PromocodeService } from 'src/app/_services/promocode/promocode.service';
import { Promocode } from 'src/app/_models/promocode';
import { NgForm } from '@angular/forms';
import {SmsService} from 'src/app/_services/sms/sms.service';

@Component({
  selector: 'app-recipient-menu-resto',
  templateUrl: './recipient-menu-resto.component.html',
  styleUrls: ['./recipient-menu-resto.component.scss']
})
export class RecipientMenuRestoComponent implements OnInit {
  /* function*/
  GetNewPrice=getNewPrice;
  GetDelivsPrice=getDelivsPrice
  @ViewChild('frame', { static: true }) public formModal;
  @ViewChild('frameV', { static: true }) public formModalV;
  @ViewChildren(GoogleComponent) child : QueryList<GoogleComponent>;
  @ViewChild('search', { static: true }) public searchElementRef: ElementRef;
valid=false;
change = readmore;
message = 0;
signInForm: FormGroup;
new: boolean = false;
new1: boolean = false;

promocodes:Promocode[]=[];
 promocode: Promocode = new Promocode();
selectedProduct : Product = new Product();
  paymentModel=new payment();
  sender:Sender;
  newdelivery:Delivery = new Delivery();
  quantity=1;
  cart :Order = new Order();
  addresse;
  addresseForm:FormGroup;
  deliveryFee:number=0;
  deliveryFeeForm:DeliveryFee= new DeliveryFee()
  payment=false;
  confirmed=false;
  md5 = new Md5();
  today= new Date();
  categories:Category[]=[];
  menu:Product[]=[];
  activeCategory;
  noteAverage=5;
  errormessage=false;
  preference:string;
  promocod: void;
  forgot = 0;
  phoneNumer="27895661"

  constructor(private route: ActivatedRoute, private deliveryFeeservice:DeliveryFeeService, private senderService:SenderService, private orderService:OrderService,private location: Location,
    private cartService:CartService, public loginService:RecipientLoginService, private deliveryService:DeliveryService, 
    private categoryServcie:CategoryService ,private noteService:NoteService,
    private promocodeService:PromocodeService, private smsService:SmsService
    
    
    
    ) {}

  ngOnInit() {

 
    this.getCart();
    this.getCategories();
    this.addresseFormBuild();
    this.getDelievryFee();
    this.confirmed=false;
    this.getPromocode();
}
getCategories(){
  this.categoryServcie.getAllcategories().subscribe(categories=>{
    this.categories=categories;
    this.getSender();
  })
}
productAvailable(product:Product){
 
 if(product.dayAvailable === undefined || product.dayAvailable === null){return true;}
 else {
   switch(product.dayAvailable){
     case 8:{return false;break;}
     case 7:{return true;break;}
     default:{if(product.dayAvailable ===this.today.getDay()){return true}else{return false}break;}
   }
 }
}
setStatus(){
  if(this.sender.times.length===0){return false;}
    let hours =new Date().getHours()  ;
    let minutes =new Date().getMinutes();
    let scheduele:Times = this.sender.times[this.today.getDay()]
    
if (scheduele.closed){return true;}
else {
  if(!scheduele.openningTime ||scheduele.openningTime=="00:00" ){return false}

let now = new Date(); now.setHours(hours,minutes,0)
 let opH =+ scheduele.openningTime.substring(0,2);
let opM =+ scheduele.openningTime.substring(3,5);
let open = new Date(); open.setHours(opH,opM,0)
let closeH =+ scheduele.closingTime.substring(0,2);
let closeM =+ scheduele.closingTime.substring(3,5);
let close = new Date(); close.setHours(closeH,closeM,0)
let midnight = new Date(); midnight.setHours(0,0,0)
if(open > close && close >= midnight){
  if(now > midnight && now > open && now > close ){return false;}
  else if(now > midnight && now < close){return false;}
  else {return true;}
}
else {
  if(now > open && now < close){
    return false;
}
else { return true}
}


}
  
    }

getDelievryFee(){
  this.deliveryFeeservice.getOneDeliveryFee().subscribe(deliveryfee=>{this.deliveryFeeForm=deliveryfee;
    this.calculateDeliveryFee()})
}
get getSignInFormCodePromo() {
  //this.email= this.signInForm.get('email')
  return this.signInForm.get('codepromo');
}

addresseFormBuild(){
this.addresseForm = new FormGroup({
arrivalPoint:new FormControl('',Validators.required),       

});
}

get getarrivalPointForm(){
  return this.addresseForm.get('arrivalPoint');
}
readUrlParams(callback) {
  this.route.queryParams.subscribe(queryParams => {
    this.route.params.subscribe(routeParams => {
      callback(routeParams, queryParams);
    });
  });
}
 getAverage(id){
  this.noteService.getNotesAverageBySender(id).subscribe(value=>{
this.noteAverage=value  })
}

getSender() {
  this.readUrlParams((routeParams, queryParams) => {
    let id = routeParams.id;
    this.getAverage(id)
    this.senderService.getSender(id).subscribe(senders => {this.sender = senders;
    senders.menu= senders.menu.filter(item=>this.productAvailable(item))
      try{
      let categoriesid = senders.menu.map(item=>{return item.category})
      if(categoriesid.length===0){
        this.menu=senders.menu;
        this.categories=[];
      }
      else {
        categoriesid = [...new Set(categoriesid)]
        this.categories=this.categories.filter(item=>categoriesid.includes(item._id))
        this.filter(this.categories[0])
        this.activeCategory=this.categories[0]
      }
    }
    catch(e){
      this.categories=[];
    }
    }); 

  });
 
}
filter(category){
  this.activeCategory=category;
  this.menu = this.sender.menu.filter(item=>item.category===category._id)
}
getCart(){
  this.readUrlParams((routeParams, queryParams) => {
    let id = routeParams.id;
    this.cart=this.cartService.getCart(id);
   
  });
}
charger(){
  this.getSender();
}
check(idO,idOL){
  this.selectedProduct.options[idO].optionLines.forEach(item=> item.checked=false);
  this.selectedProduct.options[idO].optionLines[idOL].checked=true;

}
addtocart(product:Product):void{
  console.log(product)
  let comment = "choix:";
  let supPrice =0;
  this.selectedProduct.options.forEach(option=>{
    option.optionLines.forEach(optionline => {
      comment+=(optionline.checked)?optionline.name+", ":"";
      supPrice+=(optionline.checked)?optionline.price:0;
    });
    comment+=' ';
  })
  if(this.sender.delivsFee.method===1){
    product.price-=this.GetDelivsPrice(this.sender.delivsFee,product.price) as any
  }
  let item= new OrderItem(product.name,this.quantity,product.price+supPrice,this.GetDelivsPrice(this.sender.delivsFee,product.price),comment+"Préferences: "+this.preference);  this.cartService.addToCart(this.sender._id,item);
  this.cart =  this.cartService.getCart(this.sender._id);
  console.log(this.cart)

  ;this.getSender()

 this.calculateDeliveryFee() 


}
updateCart(id,qte){
  let quantity =  this.cart.orderItems[id].quantity+qte;
  this.cart.orderItems[id].quantity=(quantity <= 0)?1:quantity;
  this.cartService.initCart(this.cart);
  this.calculateDeliveryFee()

}
deleteFromCart(id){
  this.cart.orderItems.splice(id,1)
  this.cartService.initCart(this.cart);
this.calculateDeliveryFee()

}

calculateDeliveryfee(total){

}
calculateDeliveryFee(distance?){
 /*  if(this.cart.total > this.deliveryFeeForm.orderLimit){
    this.deliveryFee = ( this.cart.total * (this.deliveryFeeForm.feePercentageOverLimit/100))
  }else{
    this.deliveryFee = ( this.cart.total * (this.deliveryFeeForm.feePercentage/100))
  } */
  this.promocodeService.getAllPromocodes().subscribe(response=>{
    this.promocodes=response;

   let res=this.promocodes.find(item=> (item.isActive===true)&&(item.code===this.promocode.code));

   console.log(res)
   if(res)
   {

    this.deliveryFee =this.deliveryFee*0.6;
    this.promocodeService.updatePromocode({...res,isActive:false}).subscribe(
      response1=>
      console.log("updated")
    )
     

   }else 
   {
    

        this.deliveryFee=this.deliveryFeeForm.feeDT
    if(distance > this.deliveryFeeForm.orderLimit){
      this.deliveryFee = this.deliveryFee + (distance-this.deliveryFeeForm.orderLimit)*this.deliveryFeeForm.priceKm
      
    }
    this.deliveryFee = +this.deliveryFee.toFixed(1)
     
   }
 
   

})
}
confirmAddress(){
  this.newdelivery.arrivalcoordinates = {latitude:this.child.first.latitude,longitude:this.child.first.longitude}
  this.newdelivery.arrivalPoint=this.searchElementRef.nativeElement.value;
  this.newdelivery.startingcoordinates= this.sender.coordinates;
  this.newdelivery.startingPoint= this.sender.address;
  this.newdelivery.distance = GoogleComponent.calculateDistance(this.newdelivery.startingcoordinates, this.newdelivery.arrivalcoordinates);
  if(this.newdelivery.distance > this.deliveryFeeForm.deliveryLimit ){
    this.errormessage=true;
  }
  else {
    this.errormessage=false;
    this.calculateDeliveryFee(this.newdelivery.distance)
    
     this.confirmed=true;
  }
  
}
commandeValidate(){
  let text=
  {
    "mail":"sinda.bououn@gmail.com ",
    "cc":"sbououn@five-consulting.com,delivs.contact@gmail.com",
    "subject":"Nouvelle Commande",
    "body":"Vous avez une nouvelle commande à verifier"
};

this.smsService.sendmail(text).subscribe(res=>{
})
 let order = this.cart;
 order.resto= this.sender._id;
 order.customer=this.loginService.getRecipient();
 this.orderService.createOrder(order).subscribe(newOrder=>{
 // this.newdelivery.price=newOrder.total*(1-this.deliveryFeeForm.profitPercentage/100);
 this.newdelivery.price=newOrder.total;
  this.newdelivery.price = +this.newdelivery.price.toFixed(1)
  this.newdelivery.delivsFee=newOrder.totalDelivs;
  this.newdelivery.sender=newOrder.resto;
  this.newdelivery.object=newOrder._id
  this.newdelivery.recipient = this.loginService.getRecipient();
  this.newdelivery.status="init";
  this.newdelivery.statusHistory.push(new history(this.newdelivery.status))
  this.newdelivery.deliveryDate = new Date();
  this.newdelivery.deliveryFee =this.deliveryFee
 // this.newdelivery.total= +(+newOrder.total + +this.deliveryFee).toFixed(1);
 this.newdelivery.total= +(+this.newdelivery.price + this.newdelivery.delivsFee + +this.deliveryFee).toFixed(1);

  if(this.payment===false){
    this.newdelivery.payment="onDelivery"
    this.deliveryService.createDelivery(this.newdelivery).subscribe(newdelivery=>{
      console.log(newdelivery)
      let notif = new Notification("Nouvelle Commande","Une nouvelle commande est arrivée","/orders/new",this.sender._id);
      this.deliveryService.sendNotification(notif).subscribe(data=>{
          this.formModal.hide();
      this.formModalV.show();
      this.cartService.deleteCart(this.sender._id)
      this.getCart();
        })
      })
      
    
  }else {
    this.newdelivery.payment="online"
    this.deliveryService.createDelivery(this.newdelivery).subscribe(newdelivery=>{
      this.cartService.deleteCart(this.sender._id)
      this.paymentModel.orderID=newdelivery._id;
      this.paymentModel.EMAIL="chaima.meliani@gmail.com";
      this.paymentModel.CustLastName="Meliani";
      this.paymentModel.CustFirstName="Chaima";
      this.paymentModel.CustTel="21175860";
      this.paymentModel.CustAddress="tunis";
      this.paymentModel.orderProducts="test";
      this.paymentModel.Amount=newdelivery.total*1000;
      this.paymentModel.signature=sha1(this.paymentModel.numSite+this.paymentModel.Password+this.paymentModel.orderID+this.paymentModel.Amount+this.paymentModel.Currency)
      this.paymentModel.Password=this.md5.appendStr(this.paymentModel.Password).end() as any;

      console.log(this.paymentModel)
      setTimeout(function(){
        var myForm = <HTMLFormElement>document.getElementById('paymentForm');
        console.log(myForm)
        myForm.submit();
      },10000)
    })
  }

  
 })
  

  
  
}
getPromocode(){
  this.promocodeService.getAllPromocodes().subscribe(response=>{
    this.promocodes=response;
    //console.log(this.promocodes)
  })
}
submit(form: NgForm) {
  this.promocodeService.getAllPromocodes().subscribe(response=>{
    this.promocodes=response;

   let res=this.promocodes.find(item=> (item.isActive===true)&&(item.code===this.promocode.code));
   console.log(res)
   //console.log(res)
   if(res)
  {
    this.new1=true;
this.new=false;
    console.log("ok")
  }else 
  {
     this.new=true;
     this.new1=false;
     console.log(this.new);
    console.log("NOT ok")

  }

  })

console.log(this.promocode.code)  
this.promocod =this.getPromocode()
   //console.log(this.promocod)
   
  }
}


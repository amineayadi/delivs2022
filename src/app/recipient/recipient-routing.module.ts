import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipientListRestoComponent } from './recipient-list-resto/recipient-list-resto.component';
import { RecipientComponent } from '../recipient/recipient.component'
import { RecipientSigninComponent } from './recipient-signin/recipient-signin.component';
import { RecipientMenuRestoComponent} from './recipient-menu-resto/recipient-menu-resto.component';
import { RecipientProfilComponent } from './recipient-profil/recipient-profil.component';
import { RecipientCommandePersoComponent } from './recipient-commande-perso/recipient-commande-perso.component';
import { RecipientHistoriqueComponent } from './recipient-historique/recipient-historique.component';
import { RecipientSuiviComComponent } from './recipient-suivi-com/recipient-suivi-com.component';
import { AuthGuardService } from '../_guards/auth-guard.service-recipient';
import { AuthGuardService2 } from '../_guards/auth-guard.service-recipient2';
import { NotesComponent } from './notes/notes.component';
import { MailConfirmationComponent } from './mail-confirmation/mail-confirmation.component' ;
import { DelivsacceuilComponent } from './delivsacceuil/delivsacceuil.component';
import { ConfidentialityComponent } from './confidentiality/confidentiality.component';


const routes: Routes = [
     {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }, 
  {
    path: '',
    component: RecipientComponent,
    children: [
/*       {
        path: '',
        component: DelivsacceuilComponent
        }, */
        {
            path: 'home',
            component: RecipientListRestoComponent
        },
        {
          path: 'login',
          component: RecipientSigninComponent
        },
        {
          path: 'confirmation/:token/:mail',
          component: MailConfirmationComponent,
          canActivate: [AuthGuardService2]
        },
        {
          path: 'restaurant/:id/:name',
          component: RecipientMenuRestoComponent
        },
        {
          path:'account' ,
          component : RecipientProfilComponent,
            canActivate: [AuthGuardService]
        },
        {
          path:'order/custom' ,
          component : RecipientCommandePersoComponent,
          canActivate: [AuthGuardService]

        },
        {
          path:'order/history' ,
          component : RecipientHistoriqueComponent,
          canActivate: [AuthGuardService]

        },
        {
          path:'order/inprogress' ,
          component : RecipientSuiviComComponent,
          canActivate: [AuthGuardService]

        },
        {
          path:'order/notes' ,
          component : NotesComponent,
          canActivate: [AuthGuardService]

        },
        {
          path:'confidentiality',
          component: ConfidentialityComponent
        }



    ]
}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipientRoutingModule { }

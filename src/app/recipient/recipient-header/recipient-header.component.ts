import { Component, OnInit, ViewChildren, QueryList, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Recipient, ConnectionInfo } from 'src/app/_models/recipient';
import { RecipientLoginService } from 'src/app/_services/recipient/recipient-login.service';
import { Router } from '@angular/router';
import { GoogleComponent } from 'src/app/google/google/google.component';
import { DataService } from 'src/app/_services/exchange/data.service';
import { GoogleService } from 'src/app/_services/google.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';

@Component({
  selector: 'recipient-header',
  templateUrl: './recipient-header.component.html',
  styleUrls: ['./recipient-header.component.scss']
})
export class RecipientHeaderComponent implements OnInit,AfterViewInit {

  @ViewChildren(GoogleComponent) child : QueryList<GoogleComponent>;
  @ViewChild('search', { static: true }) public searchElementRef: ElementRef;
  searchValue;
  message:string;

  constructor(private googleService:GoogleService,private data: DataService,public router: Router,
    private recipientService:RecipientService,public loginService:RecipientLoginService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message = message)
    setInterval(() => {
      this.update(true) 
      },300000)

  }
  update(status){
    if(this.loginService.getRecipient()){
    this.recipientService.getRecipient(this.loginService.getRecipient()).subscribe(recipient=>{
      if(recipient){
        let info = new ConnectionInfo(status)
        recipient.connectionInfo = info;
        this.recipientService.updateRecipient(recipient).subscribe(data=>{
        })
      }
     
    })
  }
   
  }
  ngAfterViewInit(): void {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.data.changeMessage({
          latitude:position.coords.latitude,
          longitude:position.coords.longitude
        })
      
      },(error)=> {
        this.googleService.getLocation().subscribe(coords=>{
          this.data.changeMessage({
            latitude:coords.location.lat,
            longitude:coords.location.lng
          })
        
        
        }) 
      });
    }
 
    
  }

  logout(){
    this.update(false)
    this.loginService.logout();
  }
  getLocation(){
    this.searchValue = this.child.first.address;
  }
  validate(){
    this.data.changeMessage({
      latitude:this.child.first.latitude,
      longitude:this.child.first.longitude
    })

  }
}

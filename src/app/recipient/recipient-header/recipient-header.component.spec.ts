import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipientHeaderComponent } from './recipient-header.component';

describe('RecipientHeaderComponent', () => {
  let component: RecipientHeaderComponent;
  let fixture: ComponentFixture<RecipientHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipientHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipientHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

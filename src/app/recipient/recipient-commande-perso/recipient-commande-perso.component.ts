import { Component, OnInit, ViewChild, NgZone, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { Delivery,history } from '../../_models/delivery';
import { DeliveryService } from '../../_services/delivery/delivery.service';
import { RecipientLoginService } from '../../_services/recipient/recipient-login.service';
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';
import { GoogleComponent } from '../../google/google/google.component';
import { DeliveryFee } from 'src/app/_models/deliveryFee';
import {Notification} from './../../_models/notification';
import { Router } from '@angular/router';
import { SmsService } from 'src/app/_services/sms/sms.service';


@Component({
  selector: 'app-recipient-commande-perso',
  templateUrl: './recipient-commande-perso.component.html',
  styleUrls: ['./recipient-commande-perso.component.scss']
})
export class RecipientCommandePersoComponent implements OnInit {
  @ViewChild('basicModal', { static: true }) basicModal: any;
  @ViewChildren(GoogleComponent) child : QueryList<GoogleComponent>;
  @ViewChild('search', { static: true }) public searchElementRef: ElementRef;
  @ViewChild('search2', { static: true }) public searchElementRef2: ElementRef;
  activeTab:number=0;
  step:number=1;
  newDelivery:Delivery= new Delivery();
  deliveryFee:DeliveryFee
valid=false;
delivsFee=0;
errorMessage=0;
phoneNumer="27895661";
  constructor(private sms:SmsService, private router : Router,private deliveryService:DeliveryService, private deliveryFeeservice:DeliveryFeeService, private loginService:RecipientLoginService) { }

  ngOnInit() {
    this.newDelivery.recipient=this.loginService.getRecipient();
    this.getDelievryFee();

 

  }

 
 getDelievryFee(){
   this.deliveryFeeservice.getOneDeliveryFee().subscribe(deliveryfee=>this.deliveryFee=deliveryfee)
 }
  changeStep(value){
    if(value==2){
      if(this.searchElementRef.nativeElement.value === undefined){this.errorMessage=1;}else {this.errorMessage=0
        this.newDelivery.startingcoordinates = {latitude:this.child.first.latitude,longitude:this.child.first.longitude};
        this.newDelivery.startingPoint=this.searchElementRef.nativeElement.value;
      this.searchElementRef = this.searchElementRef2;
      this.valid=true
      this.step=value;}
      
      
    }
    else { this.step=value;}
   

      }
  
      addDelivery(){
        if(this.searchElementRef.nativeElement.value === undefined){this.errorMessage=2;}else {this.errorMessage=0
                 this.newDelivery.arrivalcoordinates = {latitude:this.child.last.latitude,longitude:this.child.last.longitude};
        this.newDelivery.arrivalPoint=this.searchElementRef.nativeElement.value;
        this.newDelivery.distance =  GoogleComponent.calculateDistance(this.newDelivery.startingcoordinates, this.newDelivery.arrivalcoordinates);
        let fee =this.deliveryFee.feeDT;
       // let fee = ( this.newDelivery.price * (this.deliveryFee.feePercentage/100))
        if(this.newDelivery.distance > this.deliveryFee.orderLimit){
          fee = fee + (this.newDelivery.distance-this.deliveryFee.orderLimit)*this.deliveryFee.priceKm
          
        }
        fee = +fee.toFixed(1)
        this.newDelivery.delivsFee=this.newDelivery.price*(this.deliveryFee.profitPercentage/100)
        this.newDelivery.deliveryFee = fee
        this.newDelivery.status="wait";
        this.newDelivery.statusHistory.push(new history(this.newDelivery.status))
        this.newDelivery.payment = "onDelivery"
        this.newDelivery.total =  +this.newDelivery.price + +fee +this.newDelivery.delivsFee;
        this.newDelivery.deliveryDate= new Date(); 
        console.log(this.newDelivery)
      
        this.basicModal.show();

        }

 
      }
      save(){
        //this.sms.checkSms(this.phoneNumer,"test sms commande").subscribe(res=>console.log(res))
        let text=
        {
          "mail":"sinda.bououn@gmail.com ",
          "cc":"sbououn@five-consulting.com,delivs.contact@gmail.com",
          "subject":"Nouvelle Commande",
          "body":"Vous avez une nouvelle commande à verifier"
      };
      
      this.sms.sendmail(text).subscribe(res=>{
      })
         this.deliveryService.createDelivery(this.newDelivery).subscribe(Delivery=>{
          let notif = new Notification("Nouvelle livraison","Une nouvelle livraison est arrivée","/waitList","livreurs");
          this.deliveryService.sendNotification(notif).subscribe(data=>{
            this.router.navigate(["order/inprogress"]);
          })
         
        }) 
      }
}

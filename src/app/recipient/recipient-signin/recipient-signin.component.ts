import { Component, OnInit, ViewChild} from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { Recipient, ConnectionInfo } from 'src/app/_models/recipient';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { RecipientLoginService } from 'src/app/_services/recipient/recipient-login.service';
import { passwordValidator } from 'src/app/_models/custom-validators';
import { AuthService} from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';
import { SenderService } from 'src/app/_services/sender/sender.service';
import {SmsService} from 'src/app/_services/sms/sms.service';

@Component({
  selector: 'app-recipient-signin',
  templateUrl: './recipient-signin.component.html',
  styleUrls: ['./recipient-signin.component.scss']
})

export class RecipientSigninComponent implements OnInit {
  @ViewChild('basicModal', { static: true }) basicModal: any;
  @ViewChild('recipientModal', { static: true }) recipientModal: any;
  @ViewChild('modalrecipient', { static: true }) modalrecipient: any;

      /*
  0 : no error
  1:signin mail or password invalid
  2:signup mail exists in database
  3:signup mail invalid
  4:signin account not verified
  5:signup error
  6:signin success
  7:password
  */
  passimput:string;

  list: Recipient[] = [];
  Recipient: Recipient[] = [];
  Recipient2: Recipient[] = [];
  newrecipient = new Recipient()
  verif =false;
  code :string;
 message=0;
  signInForm: FormGroup;
  signUpForm: FormGroup;
  new:boolean = false;
  checkbox=false;
  recipient:Recipient=new Recipient();
  phonenumber="";
  mail="";
  forgotPassword=0;
  fieldTextType: boolean;
  fieldTextType1: boolean;
  fieldTextType2: boolean;
  forgot = 0;

  constructor(private smsService:SmsService,private senderServcie:SenderService, private deliveryFeeService:DeliveryFeeService,private authService: AuthService, private recipientService:RecipientService, private loginService:RecipientLoginService) { }

  ngOnInit() {
    this.signInFormBuild();
    this.signUpFormBuild();
    this.getRecipient();
  }
  getRecipient() {
    this.recipientService.getAllRecipients().subscribe(data => {
      this.Recipient2 = data
      //console.log(this.Recipient2)
      // console.log(deliveryMen2);
      //console.log(deliveryMen)

      /* let result=   deliveryMen2.find(function(elem){
     
     
           return elem.phoneNumber== "27895661"
         }) */
      // console.log(result)
    })
  }

    //Switching method for the password hide and show
    //chaque champ doit avoir un toggleField
    toggleFieldTextType() {
      this.fieldTextType = !this.fieldTextType;
    }
    toggleFieldTextType1(){
      this.fieldTextType1=!this.fieldTextType1;
    }
    toggleFieldTextType2(){
      this.fieldTextType2=!this.fieldTextType2;
    }

    generatePassword() {
      console.log(this.mail)
  
  //console.log(this.phoneNumber)
  this.recipientService.getRecipientByMail(this.mail).subscribe(delievryman=>{
    let recipient = new Recipient(this.signInForm.value);
  
  
      const found = this.Recipient2.find(item => item.email === recipient.email)
      console.log(found.email)
      if (!found) {
        this.message===1
      } else {
        let password = Math.random().toString(36).substr(2, 8);
        found.password=password;
  
        this.recipientService.updateRecipient(found).subscribe(data => {
          this.smsService.checkSms(data.phoneNumber, "Votre nouveau mot de passe est  " + password).subscribe(res =>{
  
            this.modalrecipient.show()
            this.signUpFormBuild();
        console.log(found)
          } )
        },
  
        )
  
      }
      /*
      
      this.deliverManService.getAllDeliveryMans().subscribe(allDelivs => {
        //console.log(allDelivs)
  
        
        //console.log(this.deliveryMen3.phoneNumber)
        if (!this.deliveryMen3) {
          this.forgotPassword = 1;
        }
        else {
          let password = Math.random().toString(36).substr(2, 8);
          this.deliveryMen3.password = password;
          this.deliverManService.updateDeliveryMan(this.deliveryMen3).subscribe(deliveryMen3 => {
            this.smsService.checkSms(deliveryMen3.phoneNumber, "le code d'activation de votre compte est  " + this.deliveryMen3.password).subscribe(res => console.log(res))
            this.Modal.show()
          })
  
        }
  */
      })
  
  
    }
  
  
  
  verification() {
    let recipient = new Recipient(this.signInForm.value);
  
  
    const found = this.Recipient2.find(item => item.email === recipient.email)
     if (found.password === this.passimput) {
      this.verif = false
      this.loginService.login(found._id,true);
  
      }
    else {
      this.verif = true
      this.forgot = 1
  
    } 
    }
  signInFormBuild(){
    this.signInForm =  new FormGroup({
      email: new FormControl('',[ Validators.email, Validators.required]),
      password: new FormControl('', Validators.required)
    });
  }

  signUpFormBuild(){
    this.signUpForm =  new FormGroup({
      firstName:new FormControl('',Validators.required),
      lastName:new FormControl('',Validators.required),
      phoneNumber: new FormControl('', [Validators.required,Validators.pattern("[0-9]{8,8}$")]),
      address: new FormControl(''),
      email: new FormControl('', [ Validators.email, Validators.required]),
      password: new FormControl('', [Validators.required,Validators.minLength(5),passwordValidator()]),
      passwordConfirmation: new FormControl('', [Validators.required,Validators.minLength(5),passwordValidator()]),
    });
  }

  get getSignInFormEmail() {
    return this.signInForm.get('email');
  }
  get getSignInFormPassword() {
    return this.signInForm.get('password');
  }

  get getSignupFormFName() {
    return this.signUpForm.get('firstName');
  }

  get getSignupFormLName() {
    return this.signUpForm.get('lastName');
  }

get getSignupFormPhone() {
  return this.signUpForm.get('phoneNumber');
}
get getSignupFormNcin() {
  return this.signUpForm.get('cin');
}
get getSignupFormAdresse () {
  return this.signUpForm.get('address');
} 
get getSignupFormEmail() {
  return this.signUpForm.get('email');
}
get getSignupFormPassword() {
  return this.signUpForm.get('password');
}
get getSignupFormPasswordC() {
  return this.signUpForm.get('passwordConfirmation');
}
onPaste(event: ClipboardEvent) {
  event.preventDefault()
 }
 checkPassword(type){
   this.message=0;
   if(type===0 && this.signUpForm.value.passwordConfirmation!="" && this.signUpForm.value.password !== this.signUpForm.value.passwordConfirmation  ){
    this.message=7;
   }
   if(type===1 && this.signUpForm.value.password!="" && this.signUpForm.value.password !== this.signUpForm.value.passwordConfirmation  ){
    this.message=7;
   }
   
 }
signin(){
let recipient= new Recipient(this.signInForm.value);
this.recipientService.getLoginRecipient(recipient.email,recipient.password).subscribe(recipient=>{
  if(recipient){
    this.message=0;   
    if(recipient.verified){
      this.loginService.login(recipient._id,true);
      this.update(recipient)
    }
    else{this.message=4;} 
 
   
  }else {
    this.message=1;
  }
 
})

}
update(recipient:Recipient){
  let connection = new ConnectionInfo(true);
  recipient.connectionInfo=connection;
  this.recipientService.updateRecipient(recipient).subscribe(data=>{
})
}

 check(){
   if(this.getSignupFormEmail.valid){
  this.deliveryFeeService.getOneDeliveryFee().subscribe(data=>{
    let value = data as any
    this.recipientService.checkMail(value.key,this.signUpForm.value.email).subscribe( data =>{
      if(data.status===200){
        this.message=0;
      }
      else {
        this.message=3
      }
    })
  })}

}


verified(){

  if(this.newrecipient.token === this.code){
    this.verif =false  
    this.loginService.login(this.newrecipient._id,true);
  }
  else{
this.verif =true    }
}
signup(){
  delete this.signUpForm.value.passwordConfirmation;
  let recipient= new Recipient(this.signUpForm.value);
  recipient.type="signup";
  this.recipientService.createRecipient(recipient).subscribe(newrecipient=>{
    this.newrecipient=newrecipient

    if(this.newrecipient ===true as any ){
      this.message=2;
    }else{ if(this.newrecipient!=null) {
      console.log('tt')
      this.message=6;
      this.smsService.checkSms(newrecipient.phoneNumber,"le code d'activation de votre compte est  "+this.newrecipient.token).subscribe(res=>console.log(res))
      this.signUpFormBuild();
      this.recipientModal.show()
      
    }}
   
  })

}

signinSocial(user:SocialUser){
  let recipient = new Recipient({'email':user.email,'password':user.id});
  this.recipientService.getLoginRecipient(recipient.email,recipient.password).subscribe(newRecipient=>{
    if(newRecipient){        
      this.message=0;   
      this.loginService.login(newRecipient._id,false)
      this.update(newRecipient)
  
    }else {
      this.message=1;
    }
   
  
  })
 
 }
 signupSocial(user:SocialUser,type){
  let recipient = new Recipient({'firstName':user.firstName,'lastName':user.lastName,'email':user.email,'password':user.id,'verified':true,'type':type });
  this.recipient=recipient;
   this.basicModal.show()
 
 }
 save(){
this.recipient.phoneNumber=this.phonenumber;
this.recipientService.createRecipientSocial(this.recipient).subscribe(newrecipient=>{
  if(newrecipient===true as any ){
   this.message=2;
  }else if(newrecipient!=null) {
   this.message=0;
   this.recipient=newrecipient;
   this.loginService.login(newrecipient._id,false)
    
  }
 })

 
 }
signWithSocial(social,action): void {
 
  let provider = (social==='FB')?FacebookLoginProvider.PROVIDER_ID:GoogleLoginProvider.PROVIDER_ID; 
  let type = (social==='FB')?"facebook":"google"; 

  this.authService.signIn(provider).then(user => {
    (action==='IN')?this.signinSocial(user):this.signupSocial(user,type)
  });
  
}




  
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipientSigninComponent } from './recipient-signin.component';

describe('RecipientSigninComponent', () => {
  let component: RecipientSigninComponent;
  let fixture: ComponentFixture<RecipientSigninComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipientSigninComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipientSigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Note } from 'src/app/_models/note';
import {NoteService} from '../../_services/note/NoteService';
import { DeliveryService } from '../../_services/delivery/delivery.service';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
  providers: [NgbRatingConfig]
})
export class NotesComponent implements OnInit {
  Note:Note[] = [] 
  constructor(private NoteService:NoteService, private DeliveryService:DeliveryService, private senderService:SenderService,private config: NgbRatingConfig) { 
    config.max=5; 
  }

  ngOnInit() {
    this.getNote();
  }

  getNote(){
    this.NoteService.getNotesByRecipient().subscribe(Note=>{
      this.Note = Note;
      this.Note.forEach((item,index)=>{this.getDelivery(index)})
});

  }

  getDelivery(index){
    let note = this.Note[index]
    this.DeliveryService.getDelivery(note.delivery).subscribe(delivery=>{
      if(delivery.sender){
        this.senderService.getSender(delivery.sender).subscribe(sender=>{
          delivery.sender=  sender as any;
          this.Note[index].delivery=delivery as any;
        });
        
      }
  
    });
  }

}

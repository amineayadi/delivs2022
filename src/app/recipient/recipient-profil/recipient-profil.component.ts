import { Component, OnInit } from '@angular/core';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { Recipient } from 'src/app/_models/recipient';
import { isUndefined } from 'util';
import { RecipientLoginService } from 'src/app/_services/recipient/recipient-login.service';

@Component({
  selector: 'app-recipient-profil',
  templateUrl: './recipient-profil.component.html',
  styleUrls: ['./recipient-profil.component.scss']
})
export class RecipientProfilComponent implements OnInit {
  recipient:Recipient=new Recipient();
  passwordUpdate:Boolean=false
  social=false;

  constructor(private recipientService:RecipientService, private loginService:RecipientLoginService) { }

  ngOnInit() {
  this.getRecipient();
  }
  getRecipient(){
    this.recipientService.getRecipient(this.loginService.getRecipient()).subscribe(recipient=>{
      this.recipient = recipient;
    });
    this.social=this.loginService.isSocial() as any
  }

  update(){
    this.recipientService.updateRecipient(this.recipient).subscribe(recipient=>{ this.recipient = recipient})

  }

}

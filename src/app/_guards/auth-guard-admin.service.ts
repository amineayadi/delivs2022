import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot ,Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SenderLoginService } from '../_services/sender/sender-login.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(
    private _router: Router,private loginService:SenderLoginService) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      let isLoggedIn = this.loginService.isLoggedInA();
      if (!isLoggedIn) {
        this._router.navigate(['admin']);
        return false;
      } else {
        return true;
      }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDashbord2Component } from './admin-dashbord2.component';

describe('AdminDashbord2Component', () => {
  let component: AdminDashbord2Component;
  let fixture: ComponentFixture<AdminDashbord2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDashbord2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDashbord2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

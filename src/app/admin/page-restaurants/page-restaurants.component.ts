
import { Component, OnInit, ViewChild } from '@angular/core';
import{ Sender, DelivsFee } from '../../_models/sender';
import { SenderService } from '../../_services/sender/sender.service';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/_services/category/category.service';
import { Category } from 'src/app/_models/category';
import { NoteService } from 'src/app/_services/note/NoteService';
import { Note } from 'src/app/_models/note';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-page-restaurants',
  templateUrl: './page-restaurants.component.html',
  styleUrls: ['./page-restaurants.component.scss'],
  providers: [NgbRatingConfig]
})
export class PageRestaurantsComponent implements OnInit {


  
  @ViewChild('frame', { static: true }) public formModal;
  id;
  sender:Sender = new Sender();
  categories:Category[]= []
  notes:Note[]=[];
  noteAverage=5;
  constructor(private route: ActivatedRoute, private senderService:SenderService ,private categoryService:CategoryService,private noteService:NoteService,private config: NgbRatingConfig) { config.max=5; }

    ngOnInit() {
      
      this.getSender();
      this.getCategories();
      
  }
  
  getCategories(){
    this.categoryService.getAllcategories().subscribe(categories=>{
      this.categories=categories;
    });
  }
  getCategorie(id){
    let cat = this.categories.find(item=>item._id===id)
    return cat.categoryName
  }
  readUrlParams(callback) {
    this.route.queryParams.subscribe(queryParams => {
      this.route.params.subscribe(routeParams => {
        callback(routeParams, queryParams);
      });
    });
  }
  getSender() {
    this.readUrlParams((routeParams, queryParams) => {
      let id = routeParams.id;
      this.senderService.getSender(id).subscribe(senders => {
        senders.delivsFee = null;
        if(senders.delivsFee === undefined || senders.delivsFee === null){senders.delivsFee=new DelivsFee();}
        this.sender = senders;
      this.getNotes(id)}); 
  
    })
  }
  update(modal){
    console.log(this.sender)
    this.senderService.updateSender(this.sender).subscribe(data=>{
      console.log(data);modal.hide()
    }) 
  }
  getNotes(id){
    this.noteService.getNotesBySender(id).subscribe(notes=>{
      this.notes=notes;
    })
    this.noteService.getNotesAverageBySender(id).subscribe(avergae=>{this.noteAverage=avergae})
  }
}
import { Component, OnInit, ViewChild } from '@angular/core';
import { Category } from 'src/app/_models/category';
import { CategoryService } from 'src/app/_services/category/category.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  @ViewChild('updateframe', { static: true })  updateframe;
  categories :Category[]=[];
  category:Category= new Category();
  constructor(private categoryService:CategoryService) { }

  ngOnInit() {
    this.getCategories();
  }
  /* get the list of categories */
  getCategories(){
    this.categoryService.getAllcategories().subscribe(categories=>{
      this.categories=categories;
      this.category= new Category();
    });
  }
  closeModal(modal){
    this.category=new Category();
    modal.hide();
  }
  openUpdateModal(category){
    this.category=category
    this.updateframe.show();
  }
  /* add a new Category */
  addCategory(){
    this.categoryService.createCategory(this.category).subscribe(newCategory=>{
      this.getCategories();
      
    });
  }
  /* update a Category */
  updateCategory(){
    this.categoryService.updateCategory(this.category).subscribe(updatedCategory=>{
      this.getCategories();
    });
  }
}

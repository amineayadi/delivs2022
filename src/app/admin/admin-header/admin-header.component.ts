import { Component, OnInit } from '@angular/core';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {

  constructor(private loginService:SenderLoginService) { }

  ngOnInit() {
  }
 logout(){
   this.loginService.logoutAdmin()
 }
}

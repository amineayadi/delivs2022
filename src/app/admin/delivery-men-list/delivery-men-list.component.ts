import { Component, OnInit, ViewChild } from '@angular/core';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import { MdbTableDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-delivery-men-list',
  templateUrl: './delivery-men-list.component.html',
  styleUrls: ['./delivery-men-list.component.scss']
})
export class DeliveryMenListComponent implements OnInit {
  @ViewChild(MdbTableDirective, { static: true }) mdbTable:MdbTableDirective;
  deliveryMen:DeliveryMan[]=[];
  list:DeliveryMan[]=[];
  deleteId="";
  p=1;
  searchText="";
  filed=0;
  fields=["lastName","firstName","email","phoneNumber"];
  constructor(private deliveryManService:DeliveryManService) { }

  ngOnInit() {
    this.getDeliveryMen()
  }
  getDeliveryMen(){
    this.deliveryManService.getAllDeliveryMans().subscribe(deliveryMen=>{
      deliveryMen.reverse()
      deliveryMen.forEach(item=>this.verifystatus(item))
      this.list=deliveryMen;
      this.deliveryMen=deliveryMen;
      this.mdbTable.setDataSource(this.list);
    })
  }
  verifystatus(deliveryMan:DeliveryMan){
    if(deliveryMan.connectionInfo){
    let today = new Date().getTime() ;
    let da = new Date(deliveryMan.connectionInfo.connectionDate).getTime()
    if(deliveryMan.connectionInfo.connected === true && (today-da) > 300000){
      this.update(deliveryMan)
    }
  }
  }
  update(deliveryMan:DeliveryMan){
    deliveryMan.connectionInfo.connected=false;
    this.deliveryManService.updateDeliveryMan(deliveryMan).subscribe(data=>console.log(data))
  }
  delete(){
    this.deliveryManService.deleteDeliveryMan(this.deleteId).subscribe(data=>this.getDeliveryMen());
  }
  search(e:string){
    if(e!=""){ 
      this.deliveryMen =this.mdbTable.searchLocalDataBy(e);
      this.mdbTable.setDataSource(this.list); 
    }
    else{
      this.getDeliveryMen();
    }
 
  }
  compareTime(value){
    let today= new Date();
    let date = new Date(value.updatedAt)
    let diff =today.getTime()-date.getTime()
    return( diff < 600000) && value.isConnected
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { DeliveryMenListComponent } from './delivery-men-list/delivery-men-list.component';
import { AdminDeliveryManComponent } from './admin-delivery-man/admin-delivery-man.component';
import { AdminRecipientComponent } from './admin-recipient/admin-recipient.component';

import {FoodDeliveriesComponent} from './food-deliveries/food-deliveries.component';
import {ListRestaurantsComponent} from './list-restaurants/list-restaurants.component';
import {PageRestaurantsComponent} from './page-restaurants/page-restaurants.component';
import {AuthentificationAdminComponent}from './authentification-admin/authentification-admin.component';
import { AjoutRestaurantComponent } from './ajout-restaurant/ajout-restaurant.component';
import { AuthGuardService } from '../_guards/auth-guard-admin.service';
import { UpdateRestaurantComponent } from './update-restaurant/update-restaurant.component';
import { DeliveryFeeComponent } from './delivery-fee/delivery-fee.component';
import { RecipientListComponent } from './recipient-list/recipient-list.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { AdminDashbordComponent } from './admin-dashbord/admin-dashbord.component';
import { AdminDashbord2Component } from './admin-dashbord2/admin-dashbord2.component';

const routes: Routes = [
  
{
  path: 'admin',
    component: AuthentificationAdminComponent
},
  {
    path: 'admin',
    component: AdminComponent,
     children: [
      {
        path: 'home',
        component: AdminDashbordComponent,
        canActivate: [AuthGuardService]
    },
    {
      path: 'homep',
      component: AdminDashbord2Component,
      canActivate: [AuthGuardService]
  },
      {
        path: 'customers',
        component: RecipientListComponent,
        canActivate: [AuthGuardService]
    },
    {
      path: 'categories',
      component: CategoryListComponent,
      canActivate: [AuthGuardService]
  },
        {
            path: 'deliveryMenList',
            component: DeliveryMenListComponent,
            canActivate: [AuthGuardService]
        },
        {
          path: 'deliveryman/:id',
            component: AdminDeliveryManComponent,
            canActivate: [AuthGuardService]
        },
{
        path: 'recipient/:id',
        component: AdminRecipientComponent,
        canActivate: [AuthGuardService]
    },
        
        {
          path: 'FoodDeliveries',
            component: FoodDeliveriesComponent,
            canActivate: [AuthGuardService]
        }
        ,
        {
          path: 'ListRestaurants',
            component: ListRestaurantsComponent,
            canActivate: [AuthGuardService]
        },
        {
          path: 'AjoutRestaurant',
            component: AjoutRestaurantComponent,
            canActivate: [AuthGuardService]
        },
        {
          path: 'ModifierRestaurant/:id',
            component: UpdateRestaurantComponent,
            canActivate: [AuthGuardService]
        }
        ,
        {
          path: 'PageRestaurants/:id',
            component: PageRestaurantsComponent,
            canActivate: [AuthGuardService]
        } ,
        {
          path: 'deliveryFee',
            component: DeliveryFeeComponent,
            canActivate: [AuthGuardService]
        }
        
        
       
        
    ]
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

import { Component, OnInit, ViewChild } from '@angular/core';
import { Delivery } from 'src/app/_models/delivery';
import { DeliveryService } from 'src/app/_services/delivery/delivery.service';
import { OrderService } from 'src/app/_services/order/order.service';
import {SenderService} from 'src/app/_services/sender/sender.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import { Sender } from 'src/app/_models/sender';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';
import { Router } from '@angular/router';
import { MdbTableDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-list-restaurants',
  templateUrl: './list-restaurants.component.html',
  styleUrls: ['./list-restaurants.component.scss']
})
export class ListRestaurantsComponent implements OnInit {

  @ViewChild(MdbTableDirective, { static: true }) mdbTable:MdbTableDirective;
  list=[]
  senders:Sender[]=[];
  deliveries:Delivery[]=[];
  deliveryMen:DeliveryMan[]=[];
  p=1;
  password="";
  pwd="";
  idSender="";
  deleteId="";
  constructor(private SenderService:SenderService,
    private loginService:SenderLoginService
  ) { }

  ngOnInit() {
    this.getRestList();
  }

  delete(){
    this.SenderService.deleteSender(this.deleteId).subscribe(res=>{
      this.getRestList();
      console.log(res)})
  }

  getRestList(){

    this.SenderService.getAllSenders().subscribe(senders => {
      
      this.senders=senders.filter(item => item.type=="0");
      this.senders.reverse();
      this.list=this.senders
      this.mdbTable.setDataSource(this.list);

    })
  }
  search(e:string){
    if(e!=""){ 
      this.senders =this.mdbTable.searchLocalDataBy(e);
      this.mdbTable.setDataSource(this.list); 
    }
    else{
      this.mdbTable.setDataSource(this.list);
    }
 
  }
  lockSender(sender:Sender,value){
    sender.closed=value;
    this.SenderService.updateSender(sender).subscribe(updatedSender=>{})
  }
  activeSender(sender:Sender,value){
    sender.active=value;
    this.SenderService.updateSender(sender).subscribe(updatedSender=>{})
  }

  generatePassword(){
     this.password = Math.random().toString(36).substr(2, 8);
     this.SenderService.getSender(this.loginService.getAdmin()).subscribe(data=>{
      this.SenderService.send(this.password,data.email).subscribe(data=>{})
    }) 
   
  }
  login(){
    if(this.pwd === this.password){
      this.password='';
        this.loginService.login(this.idSender,true)
        
    }
    
  }
}
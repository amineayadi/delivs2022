import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRecipientComponent } from './admin-recipient.component';

describe('AdminDeliveryManComponent', () => {
  let component: AdminRecipientComponent;
  let fixture: ComponentFixture<AdminRecipientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRecipientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRecipientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

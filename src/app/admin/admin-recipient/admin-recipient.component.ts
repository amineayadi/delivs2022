import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { Recipient} from 'src/app/_models/recipient';
import { DeliveryService } from 'src/app/_services/delivery/delivery.service';
import { Delivery } from 'src/app/_models/delivery';
import { PromocodeService } from 'src/app/_services/promocode/promocode.service';
import { Note } from 'src/app/_models/note';
import { Promocode } from 'src/app/_models/promocode';
import { NgForm } from '@angular/forms';
import {SmsService} from 'src/app/_services/sms/sms.service';

import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-admin-recipient',
  templateUrl: './admin-recipient.component.html',
  styleUrls: ['./admin-recipient.component.scss'],
  providers: [NgbRatingConfig]
})
export class AdminRecipientComponent implements OnInit {
    recipient:Recipient = new Recipient();
 deliveries:Delivery[]=[];
 promocodes:Promocode[]=[];
 promocode: Promocode = new Promocode();

 isLoading = false;
 noteAverage=0;
  constructor(
private router:Router,
    private smsService:SmsService,private route: ActivatedRoute,private recipientService:RecipientService,
    private deliveryService:DeliveryService,private promocodeService:PromocodeService,private config: NgbRatingConfig) { config.max=5; }

  ngOnInit() {
    this.getRecipient();
    this.getPromocode()
  }
  readUrlParams(callback) {
    this.route.queryParams.subscribe(queryParams => {
      this.route.params.subscribe(routeParams => {
        callback(routeParams, queryParams);
      });
    });
  }
   getRecipient(){
    this.readUrlParams((routeParams, queryParams) => {
      let id = routeParams.id;
      this.recipientService.getRecipient(id).subscribe(recipient => {
        this.recipient = recipient;
       
      }); 
  
    });
   }
    getPromocode(){
     this.promocodeService.getAllPromocodes().subscribe(response=>{
       this.promocodes=response;
       console.log(response)
     })
   }
  
    update(value){
     this.recipientService.updateRecipient( this.recipient).subscribe(updatedRecipient=>{
       this.recipient=updatedRecipient;
     })
     } 
    /*  block(value){
      this.deliveryman.available=value;
      this.deliverymanService.updateDeliveryMan( this.deliveryman).subscribe(updatedDeliveryMan=>{
        this.deliveryman=updatedDeliveryMan;
        this.deliverymanService.deliveryManwelocmeMail(updatedDeliveryMan._id).subscribe(data=>{
        })
      })
      } */




      submit(form: NgForm) {
        if (!form.invalid) {
         
          this.promocodeService.createPromocode(this.promocode).subscribe(response => {
            console.log(response)
            this.recipientService.updateRecipient(this.recipient).subscribe(data => {
              this.smsService.checkSms(data.phoneNumber, "Votre code promo est  " + this.promocode.code).subscribe(res =>{
                this.router.navigate(["/admin/customers"]);
              } )
            },
      
            )
      
          });
        }
      }
}


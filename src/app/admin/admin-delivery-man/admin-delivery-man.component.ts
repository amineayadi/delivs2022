import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import { DeliveryService } from 'src/app/_services/delivery/delivery.service';
import { Delivery } from 'src/app/_models/delivery';
import { NoteService } from 'src/app/_services/note/NoteService';
import { Note } from 'src/app/_models/note';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-admin-delivery-man',
  templateUrl: './admin-delivery-man.component.html',
  styleUrls: ['./admin-delivery-man.component.scss'],
  providers: [NgbRatingConfig]
})
export class AdminDeliveryManComponent implements OnInit {
 deliveryman:DeliveryMan = new DeliveryMan();
 deliveries:Delivery[]=[];
 notes:Note[]=[];
 noteAverage=0;
  constructor(private route: ActivatedRoute,private deliverymanService:DeliveryManService,
    private deliveryService:DeliveryService,private noteService:NoteService,private config: NgbRatingConfig) { config.max=5; }

  ngOnInit() {
    this.getDeliveryMan();
  }
  readUrlParams(callback) {
    this.route.queryParams.subscribe(queryParams => {
      this.route.params.subscribe(routeParams => {
        callback(routeParams, queryParams);
      });
    });
  }
   getDeliveryMan(){
    this.readUrlParams((routeParams, queryParams) => {
      let id = routeParams.id;
      this.deliverymanService.getDeliveryMan(id).subscribe(deliveryman => {
        this.deliveryman = deliveryman;
        this.getDeliveries(this.deliveryman._id)
        this.getNotes(this.deliveryman._id)
      }); 
  
    });
   }
    getNotes(id){
     this.noteService.getNotesByDeliveryman(id).subscribe(notes=>{
       this.notes=notes;
     })
     this.noteService.getNotesAverageByDeliveryman(id).subscribe(avergae=>{this.noteAverage=avergae})
   }
   getDeliveries(id){
    this.deliveryService.getDeliveryByDelievryManAndStatus(id,"delivered").subscribe(deliveries=>{
      this.deliveries=deliveries;
    })
 
  }
   update(value){
     this.deliveryman.active=value;
     this.deliverymanService.updateDeliveryMan( this.deliveryman).subscribe(updatedDeliveryMan=>{
       this.deliveryman=updatedDeliveryMan;
     })
     }
     block(value){
      this.deliveryman.available=value;
      this.deliverymanService.updateDeliveryMan( this.deliveryman).subscribe(updatedDeliveryMan=>{
        this.deliveryman=updatedDeliveryMan;
        this.deliverymanService.deliveryManwelocmeMail(updatedDeliveryMan._id).subscribe(data=>{
        })
      })
      }
}

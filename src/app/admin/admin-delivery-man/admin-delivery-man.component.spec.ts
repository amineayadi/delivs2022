import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDeliveryManComponent } from './admin-delivery-man.component';

describe('AdminDeliveryManComponent', () => {
  let component: AdminDeliveryManComponent;
  let fixture: ComponentFixture<AdminDeliveryManComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDeliveryManComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDeliveryManComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

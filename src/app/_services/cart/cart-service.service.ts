import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Order } from 'src/app/_models/order';
@Injectable({
  providedIn: 'root'
})
export class CartService {
 


  constructor(private cookieService: CookieService) { }
  initCart(order:Order){
    order.total=this.getTotal(order.orderItems)
    order.totalDelivs=this.getDelivsTotal(order.orderItems)
    localStorage.removeItem(order.resto);
    localStorage.setItem(order.resto,JSON.stringify(order));
    console.log(this.getCart(order.resto))
    
  }
  deleteCart(id){
    localStorage.removeItem(id);
    
  }
  cartExist(id):Boolean{
    return localStorage.getItem(id)!==null;
     
  }

  getCart(id):Order{
    if(this.cartExist(id)){
    return new Order(JSON.parse(localStorage.getItem(id)));
    }
 else {   let order = new Order() ; order.total=0;order.totalDelivs=0 ; return order;}
  }
  
  addToCart(id,item){
    console.log(111)
    let o = this.getCart(id);
    console.log(o)
    o.resto=id;
    o.orderItems = [...o.orderItems, item];
    o.total = this.getTotal(o.orderItems );
    o.totalDelivs = this.getDelivsTotal(o.orderItems)
    console.log(o)
    this.initCart(o);
  }
  getTotal(items):number{
    let total = 0;
    items.forEach(item => {
      total+=item.price*item.quantity
    });
    return total;
  }
  getDelivsTotal(items):number{
    let total = 0;
    items.forEach(item => {
      total+=item.delivs*item.quantity
    });
    return total;
  }
 
}

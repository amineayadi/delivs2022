import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import { Category } from 'src/app/_models/category';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  env : Env = new Env();
  private Url = this.env.host+'/categoryservice/';
  constructor(private http: HttpClient) { }
       /** POST: add a new Category for the first time */
       createCategory (category: Category): Observable<Category> {

        return this.http.post<Category>(this.Url+'category', category)
          .pipe(
            catchError(this.handleError<Category>('createCategory', category))
          );
      }
           /** PUT: update Category   */
           updateCategory (category: Category): Observable<Category> {
            return this.http.put<Category>(this.Url+'category/'+category._id, category)
              .pipe(
                catchError(this.handleError<Category>('updateCategory', category))
              );
          }
                       /** GET: get one Category By ID   */
        getCategory (id: string): Observable<Category> {
          return this.http.get<Category>(this.Url+'category/'+id)
            .pipe(
              catchError(this.handleError<Category>('getCategory', null))
            );
        }
      /** GET: delete one Category By ID   */
      deleteCategory (id: string): Observable<Category> {
       return this.http.delete<Category>(this.Url+'category/'+id)
    .pipe(
        catchError(this.handleError<Category>('deleteCategory', null))
        );
      }

        /** GET: get all Category   */
        getAllcategories (): Observable<Category[]> {
          return this.http.get<Category[]>(this.Url+'category')
            .pipe(
              catchError(this.handleError<Category[]>('getAllcategories', []))
            );
        }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
  //    console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
  //    console.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

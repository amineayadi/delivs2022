import { TestBed } from '@angular/core/testing';

import { DeliveryFeeService } from './delivery-fee.service';

describe('DeliveryFeeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeliveryFeeService = TestBed.get(DeliveryFeeService);
    expect(service).toBeTruthy();
  });
});

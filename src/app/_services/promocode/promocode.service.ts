import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Env } from '../env';
import { Promocode } from '../../_models/promocode';
import { RecipientLoginService } from '../recipient/recipient-login.service';
import { DeliveryManLoginService } from '../deliveryMan/delivery-login.service';
@Injectable({
  providedIn: 'root'
})
export class PromocodeService {
  env: Env = new Env();
  private Url = this.env.host + '/promocodeservice/';
  constructor(private http: HttpClient) { }
  /** POST: add a new Promocode  */
  createPromocode(promocode: Promocode): Observable<Promocode> {
    return this.http.post<Promocode>(this.Url + 'promocode', promocode)
      .pipe(catchError(this.handleError<Promocode>('createPromocode', promocode)));
  }
  /** PUT: update Promocode   */
  updatePromocode(promocode: Partial<Promocode>): Observable<Partial<Promocode>> {
    return this.http.put <Partial<Promocode>>(this.Url + 'promocode/' + promocode._id, promocode)
      .pipe(catchError(this.handleError<Partial<Promocode>>('updatePromocode', promocode)));
  }
  /** GET: get one Promocode   */
  getPromocode(id: string): Observable<Promocode> {
    return this.http.get<Promocode>(this.Url + 'promocode/' + id)
      .pipe(catchError(this.handleError<Promocode>('getPromocode', null)));
  }
  /** GET: get all Promocodes   */
  getAllPromocodes(): Observable<Promocode[]> {
    return this.http.get<Promocode[]>(this.Url + 'promocode')
      .pipe(catchError(this.handleError<Promocode[]>('getAllPromocodes', [])));
  }
    
  /** GET: delete  Promocode   */
  deletePromocode(id: string): Observable<Promocode> {
    return this.http.delete<Promocode>(this.Url + 'promocode/' + id)
      .pipe(catchError(this.handleError<Promocode>('deletePromocode', null)));
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
    //  console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
    //  console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  DeliveryMan } from 'src/app/_models/deliveryMan';
@Injectable({
  providedIn: 'root'
})
export class DeliveryManService {

  env : Env = new Env();
  path=location.origin+"/deliveryman/confirmation"
  private Url = this.env.host+'/deliverymanservice/';
  constructor(private http: HttpClient) { }
       /** GET: checkMail */
       checkMail(key,mail): Observable<any> {
      
        return this.http.get<any>("https://api.email-validator.net/api/verify?APIKey="+key+"&EmailAddress="+mail)
        .pipe(
          catchError(err => {
       //     console.log('caught mapping error and rethrowing', err);
            return throwError(err );
        })
        )
     
  
   }
       /** POST: uploadImage */
    uploadImage(image: any): Observable<any> {
      const formdata: FormData = new FormData();
      
        formdata.append('image', image);
        return this.http.post<any>(this.Url+'upload', formdata,{responseType:"text" as "json"})
    .pipe(
      catchError(err => {
     //   console.log('caught mapping error and rethrowing', err);
        return throwError(err );
    })
    )
   
    }
       /** POST: add a new DeliveryMan  */
       createDeliveryMan (deliveryMan: any): Observable<DeliveryMan> {
      /*   const formdata: FormData = new FormData();
        for ( var key in deliveryMan ) {
          formdata.append(key, deliveryMan[key]);
      }  formdata.append('url',this.path)*/
      deliveryMan.url = this.path as any
     
        return this.http.post<DeliveryMan>(this.Url+'DeliveryMan', deliveryMan)
          .pipe(
            catchError(this.handleError<DeliveryMan>('createDeliveryMan', deliveryMan))
          );
      }
       /** POST: add a new DeliveryMan  */
       createDeliveryManSocial (deliveryMan: DeliveryMan): Observable<DeliveryMan> {
        return this.http.post<DeliveryMan>(this.Url+'DeliveryManSocial', deliveryMan)
          .pipe(
            catchError(this.handleError<DeliveryMan>('createDeliveryMan', deliveryMan))
          );
      }
        /** POST: Confirm mail  */
       confirmDeliveryManMail (token: string,mail:string): Observable<DeliveryMan> {
      
        return this.http.post<DeliveryMan>(this.Url+'confirmMail', {'token':token,'email':mail,'url':this.path})
          .pipe(
            catchError(this.handleError<DeliveryMan>('confirmDeliveryManMail', null))
          );
      }
     /** GET: Confirm mail  */
    confirmDeliveryManMailSocial (id: string): Observable<boolean> {
     return this.http.get<boolean>(this.Url+'confirmMail/'+id)
     .pipe(
     catchError(this.handleError<boolean>('confirmDeliveryManMail', null))
    );
    }
          /** POST: Confirm mail  */
          deliveryManwelocmeMail (id: string): Observable<DeliveryMan> {
      
            return this.http.post<DeliveryMan>(this.Url+'welcomeMail', {'id':id})
              .pipe(
                catchError(this.handleError<DeliveryMan>('deliveryManwelocmeMail', null))
              );
          }
           /** PUT: update DeliveryMan   */
        updateDeliveryMan (deliveryMan: DeliveryMan): Observable<DeliveryMan> {
            return this.http.put<DeliveryMan>(this.Url+'DeliveryMan/'+deliveryMan._id, deliveryMan)
              .pipe(
                catchError(this.handleError<DeliveryMan>('updateDeliveryMan', deliveryMan))
              );
          }
                     /** PUT: update DeliveryMan'location   */
        updateDeliveryManLocation (deliveryMan: DeliveryMan): Observable<DeliveryMan> {
          return this.http.put<DeliveryMan>(this.Url+'DeliveryManLocation/'+deliveryMan._id, deliveryMan)
            .pipe(
              catchError(this.handleError<DeliveryMan>('updateDeliveryMan', deliveryMan))
            );
        }
             /** GET: get one DeliveryMan   */
        getDeliveryMan (id: string): Observable<DeliveryMan> {
          return this.http.get<DeliveryMan>(this.Url+'DeliveryMan/'+id)
            .pipe(
              catchError(this.handleError<DeliveryMan>('getDeliveryMan', null))
            );
        }
              /** GET: get all DeliveryMans   */
        getAllDeliveryMans (): Observable<DeliveryMan[]> {
          return this.http.get<DeliveryMan[]>(this.Url+'DeliveryMan')
            .pipe(
              catchError(this.handleError<DeliveryMan[]>('getAllDeliveryMans', []))
            );
        }
        /** GET: Login  */
       getLoginDeliveryMan (mail,password): Observable<DeliveryMan> {
       return this.http.get<DeliveryMan>(this.Url+'login/'+mail+'/'+password)
        .pipe(
         catchError(this.handleError<DeliveryMan>('getLoginDeliveryMan', null))
          );
        }
          /** GET: ByMail  */
       getDeliveryManByMail (mail): Observable<DeliveryMan> {
        return this.http.get<DeliveryMan>(this.Url+'getByMail/'+mail)
         .pipe(
          catchError(this.handleError<DeliveryMan>('getDeliveryManByMail', null))
           );
         }
             /** DELETE: delete one DeliveryMan   */
       deleteDeliveryMan (id: string): Observable<DeliveryMan> {
      return this.http.delete<DeliveryMan>(this.Url+'DeliveryMan/'+id)
        .pipe(
      catchError(this.handleError<DeliveryMan>('deleteDeliveryMan', null))
       );
            }
        private handleError<T> (operation = 'operation', result?: T) {
          return (error: any): Observable<T> => {
       
            // TODO: send the error to remote logging infrastructure
         //   console.error(error); // log to console instead
       
            // TODO: better job of transforming error for user consumption
        //    console.log(`${operation} failed: ${error.message}`);
       
            // Let the app keep running by returning an empty result.
            return of(result as T);
          };
        }
}

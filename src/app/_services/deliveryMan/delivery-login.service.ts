import { Injectable } from '@angular/core';
import SimpleCrypto from "simple-crypto-js";
import { Router } from '@angular/router';
import { AuthService} from "angularx-social-login";

@Injectable({
  providedIn: 'root'
})
export class DeliveryManLoginService {
  _secretKey = "Five12345";
  simpleCrypto = new SimpleCrypto(this._secretKey);
  constructor(private router : Router,private authService: AuthService) { }
  encrypt(object){

    return  this.simpleCrypto.encrypt(object);
  }
  decrypt(object):any{
    return this.simpleCrypto.decrypt(object); 
  }
  login(id:string,login:Boolean){

      sessionStorage.setItem('isLoggedInD','true');
      sessionStorage.setItem('Delivery',this.encrypt(id));
      (!login)?sessionStorage.setItem('social','true'):"";
        this.router.navigate(["deliveryman/account"])
        
      
  }
  getDeliveryMan():string{
    let deliveryId:string;

    deliveryId =sessionStorage.getItem('Delivery')     
  
    if(deliveryId){ return this.decrypt(deliveryId);}
      else{return null;}
    
  }
  isLoggedIn():Boolean{

        return Boolean(JSON.parse(sessionStorage.getItem('isLoggedInD'))); 

  }
  isSocial():Boolean{

    return Boolean(JSON.parse(sessionStorage.getItem('social'))); 

}
  logout() {

        sessionStorage.removeItem('isLoggedInD');
        sessionStorage.removeItem('Delivery');
        if(this.isSocial()){
          sessionStorage.removeItem('social');
          this.authService.signOut();
        }
      this.router.navigate(["deliveryman"]);
  }
}

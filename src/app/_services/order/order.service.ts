import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  Order } from 'src/app/_models/order';
import { payment } from 'src/app/_models/payment';
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  env : Env = new Env();
  private Url = this.env.host+'/orderservice/';
  constructor(private http: HttpClient) { }
    /** POST: add a new Order  */
    payOrder (payment: payment): Observable<payment> {
      return this.http.post<payment>('https://preprod.gpgcheckout.com/Paiement_test/Validation_paiement.php', payment)
        .pipe(
          catchError(this.handleError<payment>('payOrder', payment))
        );
    }
         /** POST: add a new Order  */
         createOrder (order: Order): Observable<Order> {
          return this.http.post<Order>(this.Url+'order', order)
            .pipe(
              catchError(this.handleError<Order>('createOrder', order))
            );
        }
             /** PUT: update Order   */
          updateOrder (order: Order): Observable<Order> {
              return this.http.put<Order>(this.Url+'order/'+order._id, order)
                .pipe(
                  catchError(this.handleError<Order>('updateOrder', order))
                );
            }
               /** GET: get one Order   */
          getOrder (id: string): Observable<Order> {
            return this.http.get<Order>(this.Url+'order/'+id)
              .pipe(
                catchError(this.handleError<Order>('getOrder', null))
              );
          }
                /** GET: get all Orders   */
          getAllOrders (): Observable<Order[]> {
            return this.http.get<Order[]>(this.Url+'order')
              .pipe(
                catchError(this.handleError<Order[]>('getAllOrders', []))
              );
          }
               /** GET: delete  Order   */
         deleteOrder (id: string): Observable<Order> {
        return this.http.delete<Order>(this.Url+'order/'+id)
          .pipe(
        catchError(this.handleError<Order>('deleteOrder', null))
         );
              }
          private handleError<T> (operation = 'operation', result?: T) {
            return (error: any): Observable<T> => {
         
              // TODO: send the error to remote logging infrastructure
              // console.error(error); // log to // console instead
         
              // TODO: better job of transforming error for user consumption
              // console.log(`${operation} failed: ${error.message}`);
         
              // Let the app keep running by returning an empty result.
              return of(result as T);
            };
          }
}

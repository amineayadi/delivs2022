import { Injectable } from '@angular/core';
import SimpleCrypto from "simple-crypto-js";
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class SenderLoginService {
  _secretKey = "Five12345";
  simpleCrypto = new SimpleCrypto(this._secretKey);
  constructor(private router : Router) { }
  encrypt(object){

    return  this.simpleCrypto.encrypt(object);
  }
  decrypt(object):any{
    return this.simpleCrypto.decrypt(object); 
  }
  login(id:string,login:Boolean){

      sessionStorage.setItem('isLoggedInS','true');
      sessionStorage.setItem('sender',this.encrypt(id));

        (login)?this.router.navigate(["/restaurant/account"]):location.reload();
        
      
  }
  loginAdmin(id:string){

    sessionStorage.setItem('isLoggedInA','true');
    sessionStorage.setItem('admin',this.encrypt(id));
    this.router.navigate(["admin/home"])
      
    
}
  getSender():string{
    let senderId:string;

    senderId =sessionStorage.getItem('sender')     
  
    if(senderId){ return this.decrypt(senderId);}
      else{return null;}
    
  }
  getAdmin():string{
    let senderId:string;

    senderId =sessionStorage.getItem('admin')     
  
    if(senderId){ return this.decrypt(senderId);}
      else{return null;}
    
  }
  isLoggedIn():Boolean{

        return Boolean(JSON.parse(sessionStorage.getItem('isLoggedInS'))); 

  }
  isLoggedInA():Boolean{

    return Boolean(JSON.parse(sessionStorage.getItem('isLoggedInA'))); 

}
  logout() {

        sessionStorage.removeItem('isLoggedInS');
        sessionStorage.removeItem('sender');
    if(this.isLoggedInA()){
      this.router.navigate(["/admin/ListRestaurants"]);
    }else {
      this.router.navigate(["restaurant"]);
    }
     
  }
  logoutAdmin() {

    sessionStorage.removeItem('isLoggedInS');
    sessionStorage.removeItem('sender');

  this.router.navigate(["admin"]);
}
}

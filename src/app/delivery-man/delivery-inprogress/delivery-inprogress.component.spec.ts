import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryInprogressComponent } from './delivery-inprogress.component';

describe('DeliveryInprogressComponent', () => {
  let component: DeliveryInprogressComponent;
  let fixture: ComponentFixture<DeliveryInprogressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryInprogressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryInprogressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

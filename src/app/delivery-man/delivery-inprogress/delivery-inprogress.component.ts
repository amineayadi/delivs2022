import { Component, OnInit, ViewChild } from '@angular/core';
import { Delivery ,history} from 'src/app/_models/delivery';
import {DeliveryService} from '../../_services/delivery/delivery.service';
import { DeliveryManLoginService } from 'src/app/_services/deliveryMan/delivery-login.service';
import { OrderService } from 'src/app/_services/order/order.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { readmore } from './../../_services/truncate'
import {Notification} from './../../_models/notification';
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';

@Component({
  selector: 'app-delivery-inprogress',
  templateUrl: './delivery-inprogress.component.html',
  styleUrls: ['./delivery-inprogress.component.scss']
})
export class DeliveryInprogressComponent implements OnInit {
  price=0;
  @ViewChild('framee', { static: true })  formModal;
  selectedId="";
  Delivery:Delivery[] = []
  change = readmore;
  status=[
    {id:"affected",text:"Confirmation prix"}, 
    { id:"accepted",text:"en cours de préparation"},
   { id:"updated",text:"Confirmation client"},
   { id:"onWay",text:"En route"},
  ]
  deliveryFee;
  constructor(private DeliveryService:DeliveryService,private loginService:DeliveryManLoginService,private deliveryfeeService:DeliveryFeeService,
    private orderService:OrderService,private recipientService:RecipientService,private senderService:SenderService) { }

  ngOnInit() {
    this.getDelivery();
  }
  getDelievryFee(){
    this.deliveryfeeService.getOneDeliveryFee().subscribe(deliveryfee=>this.deliveryFee=deliveryfee)
  }
  find(status){
    let item = this.status.find(item=>item.id===status)
    return item.text
  }
  getDelivery(){
    this.DeliveryService.getDeliveryByDelievryManAndStatus(this.loginService.getDeliveryMan(),"affected,accepted,updated,onWay").subscribe(Delivery=>{
      this.Delivery = Delivery;
      this.getDelievryFee();
      this.Delivery.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index),this.getSender(index)})

    });

  }
  getSender(id){
    let delivery = this. Delivery[id];
    if(delivery.object){
      this.senderService.getSender(delivery.sender).subscribe(sender=>{
        delivery.sender=  sender as any
      });
    }
  }
  getOrder(id){
    let delivery = this. Delivery[id];
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }
    getRecipient(id){
      let delivery = this. Delivery[id];
      if(delivery.recipient){
        this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
          delivery.recipient=  recipient.firstName + " "+ recipient.lastName.slice(0,1) + " ("+ recipient.phoneNumber+")";
        });
      }  
    }


  deliver(id){
    this.DeliveryService.getDelivery(id).subscribe(delivery=>{
      delivery.status="onWay";
      delivery.statusHistory.push(new history(delivery.status));
      this.DeliveryService.updateDelivery(delivery).subscribe(updatedDelivery=>{
        this.sendNotif("La commande est en route","/recipient/orders",updatedDelivery.recipient)
        this.getDelivery()
      })
    }) 
  }
  delivered(){
    this.DeliveryService.getDelivery(this.selectedId).subscribe(delivery=>{
      delivery.status="delivered";
      delivery.statusHistory.push(new history(delivery.status));
      this.DeliveryService.updateDelivery(delivery).subscribe(updatedDelivery=>{
        this.sendNotif("Votre commande est arrivée","/recipient/orders",updatedDelivery.recipient)
        this.getDelivery()
      })
    }) 
  }
  confirm(){
    this.DeliveryService.getDelivery(this.selectedId).subscribe(delivery=>{
      delivery.status="accepted";
      delivery.statusHistory.push(new history(delivery.status));
      this.DeliveryService.updateDelivery(delivery).subscribe(updatedDelivery=>{
        this.sendNotif("La commande est en cours de preparation","/recipient/orders",updatedDelivery.recipient)

     
          this.getDelivery()
        
      })
    }) 
  }
  update(){
    this.DeliveryService.getDelivery(this.selectedId).subscribe(delivery=>{
      let delivsFee = this.price*(this.deliveryFee.profitPercentage/100);
      delivsFee = +delivsFee.toFixed(1);
      delivery.price=+this.price;
      delivery.delivsFee=delivsFee
      delivery.total=+delivery.deliveryFee+delivsFee+delivery.price;
      delivery.total = +delivery.total.toFixed(1);
      delivery.status="updated";
      delivery.statusHistory.push(new history(delivery.status));
       this.DeliveryService.updateDelivery(delivery).subscribe(updatedDelivery=>{
        this.sendNotif("Le prix de la commande est mis a jour","/recipient/orders",updatedDelivery.recipient)
     
          this.getDelivery()
        
      }) 
    }) 
  }
  sendNotif(message,route,destination){
    let notif2 = new Notification("Nouvelle commande",message,route,destination);
        this.DeliveryService.sendNotification(notif2).subscribe(data=>{
        })
  }



}

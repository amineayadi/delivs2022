
import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DeliveryMan } from 'src/app/_models/deliveryMan';

import { DeliveryManLoginService } from 'src/app/_services/deliveryMan/delivery-login.service';
import { passwordValidator } from 'src/app/_models/custom-validators';
import { GoogleService } from 'src/app/_services/google.service';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';
import { ConnectionInfo } from 'src/app/_models/recipient';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { SmsService } from 'src/app/_services/sms/sms.service';
import { PromocodeService } from 'src/app/_services/promocode/promocode.service';
import { Promocode } from 'src/app/_models/promocode';


@Component({
  selector: 'app-delivery-man-signin',
  templateUrl: './delivery-man-signin.component.html',
  styleUrls: ['./delivery-man-signin.component.scss']
})
export class DeliveryManSigninComponent implements OnInit {
  @ViewChild('basicModal', { static: true }) basicModal: any;
  @ViewChild('Modal', { static: true }) Modal: any;
  @ViewChild('Modal2', { static: true }) Modal2: any;
  @ViewChild('tops', { static: true }) tops: any;
  @ViewChild('frame', { static: true }) frame: any;

  public innerWidth: any;
  /*
0 : no error
1:signin mail or password invalid
2:signup mail exists in database
3:signup mail invalid
4:signin account not verified
5:signup error
6:signin success
7:code invalid
*/
fieldTextType: boolean;
  fieldTextType1: boolean;

password1:string;
email:string;
  message = 0;
  code: string;
  phoneNumber: string;
  passimput:string;
  password: string;
  signInForm: FormGroup;
  signUpForm: FormGroup;
  new: boolean = false;
  selectedFiles: FileList;
  delievryman: DeliveryMan = new DeliveryMan();
  checkbox = false;
  phonenumber = "";
  mail = "";
  forgotPassword = 0;
  forgot = 0;

  list: DeliveryMan[] = [];
  newdeliverman = new DeliveryMan()
  deliveryMen: DeliveryMan[] = [];
  deliveryMen2: DeliveryMan[] = [];
  Promocode2: Promocode[] = [];

  deliveryMen3 = new DeliveryMan();
  feresText:String;
  verif = false;
  constructor(private deliveryManService: DeliveryManService,
    private smsService: SmsService, private senderServcie: SenderService,
    private deliveryFeeService: DeliveryFeeService, private authService: AuthService,
    private googleService: GoogleService, private deliverManService: DeliveryManService, private loginService: DeliveryManLoginService,
    private promocodeService:PromocodeService
    
    ) { }

  ngOnInit() {
    console.log(location)
    this.innerWidth = window.innerWidth;
    this.signInFormBuild();
    this.signUpFormBuild();
    this.getDeliveryMen()
    console.log(this.signInForm.get('email').value)
  
this.getPromocode()
  }
  getPromocode() {
    this.promocodeService.getAllPromocodes().subscribe(data => {
      this.Promocode2 = data
      console.log(this.Promocode2)
      // console.log(deliveryMen2);
      //console.log(deliveryMen)

      /* let result=   deliveryMen2.find(function(elem){
     
     
           return elem.phoneNumber== "27895661"
         }) */
      // console.log(result)
    })
  }
  getDeliveryMen() {
    this.deliveryManService.getAllDeliveryMans().subscribe(data => {
      this.deliveryMen2 = data
      //console.log(deliveryMen)
      // console.log(deliveryMen2);
      //console.log(deliveryMen)

      /* let result=   deliveryMen2.find(function(elem){
     
     
           return elem.phoneNumber== "27895661"
         }) */
      // console.log(result)
    })
  }

  //permet de générer une nouvelle mot de passe "Mot de passe oublié"
  // l'envoie de la nouvelle mot de passe sera modifer par téléphone au lieu du mail
  generatePassword() {
    console.log(this.mail)

//console.log(this.phoneNumber)
this.deliverManService.getDeliveryManByMail(this.mail).subscribe(delievryman=>{
  let deliverMan = new DeliveryMan(this.signInForm.value);


    const found = this.deliveryMen2.find(item => item.email === deliverMan.email)
    console.log(found.email)
    if (!found) {
      this.message===1
    } else {
      let password = Math.random().toString(36).substr(2, 8);
      found.password=password;

      this.deliverManService.updateDeliveryMan(found).subscribe(data => {
        this.smsService.checkSms(data.phoneNumber, "Votre nouveau mot de passe est  " + password).subscribe(res =>{

          this.tops.show()
          this.signUpFormBuild();
      console.log(found)
        } )
      },

      )

    }
    /*
    
    this.deliverManService.getAllDeliveryMans().subscribe(allDelivs => {
      //console.log(allDelivs)

      
      //console.log(this.deliveryMen3.phoneNumber)
      if (!this.deliveryMen3) {
        this.forgotPassword = 1;
      }
      else {
        let password = Math.random().toString(36).substr(2, 8);
        this.deliveryMen3.password = password;
        this.deliverManService.updateDeliveryMan(this.deliveryMen3).subscribe(deliveryMen3 => {
          this.smsService.checkSms(deliveryMen3.phoneNumber, "le code d'activation de votre compte est  " + this.deliveryMen3.password).subscribe(res => console.log(res))
          this.Modal.show()
        })

      }
*/
    })


  }



verification() {
  let deliverMan = new DeliveryMan(this.signInForm.value);


  const found = this.deliveryMen2.find(item => item.email === deliverMan.email)
   if (found.password === this.passimput) {
    this.verif = false
this.update(found, true)

    }
  else {
    this.verif = true
    this.forgot = 1

  } 
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }
  ngAfterViewChecked() {


  }
  // permet de créer le formulaire d'authentification
  signInFormBuild() {
    this.signInForm = new FormGroup({
      email: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', Validators.required)
    });
    console.log(this.signInForm.get('email').value)


  }
  // permet de créer le formulaire d'inscription
  signUpFormBuild() {
    this.signUpForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.email, Validators.required]),
      address: new FormControl('', Validators.required),
      /*      cin: new FormControl('', [Validators.required,Validators.pattern("[0-9]{8,8}$")]),
           cinImage: new FormControl('', [Validators.required]), */
      phoneNumber: new FormControl('', [Validators.required, Validators.pattern("[0-9]{8,8}$")]),
      password: new FormControl('', [Validators.required, Validators.minLength(5), passwordValidator()]),
      passwordConfirmation: new FormControl('', [Validators.required, Validators.minLength(5), passwordValidator()]),
    });

  }
  // les getters des attributs du formulaire
  get getSignInFormEmail() {
    //this.email= this.signInForm.get('email')
    return this.signInForm.get('email');
  }
  get getSignInFormPassword() {
    return this.signInForm.get('password');
  }
  get getSignupFormName() {
    return this.signUpForm.get('firstName');
  }

  get getSignupFormEmail() {
    return this.signUpForm.get('email');
  }
  get getSignUpFormCin() {
    return this.signUpForm.get('cin');
  }
  get getSignUpFormCinImage() {
    return this.signUpForm.get('cinImage');
  }
  get getSignUpFormTel() {
    return this.signUpForm.get('phoneNumber');
  }

  get getSignupFormPassword() {
    return this.signUpForm.get('password');
  }

  get getSignupFormLastName() {
    return this.signUpForm.get('lastName');
  }
  get getSignupFormAddress() {
    return this.signUpForm.get('address');
  }
  get getSignupFormPasswordC() {
    return this.signUpForm.get('passwordConfirmation');
  }
  //permet de désactiver le fonctionnement du copier coller dans les input des mot de passes
  onPaste(event: ClipboardEvent) {
    event.preventDefault()
  }
  //permet de vérifer que les deux mot de passes saisis sont identiques 
  checkPassword(type) {
    this.message = 0;
    if (type === 0 && this.signUpForm.value.passwordConfirmation != "" && this.signUpForm.value.password !== this.signUpForm.value.passwordConfirmation) {
      this.message = 7;
    }
    if (type === 1 && this.signUpForm.value.password != "" && this.signUpForm.value.password !== this.signUpForm.value.passwordConfirmation) {
      this.message = 7;
    }

  }
  /*   selectFile(event) {
      this.selectedFiles = event.target.files;
      this.signUpForm.get('cinImage').setValue(this.selectedFiles.item(0).name) 
    } */
  //permet de faire l'authentification 
  signin() {

    let deliverMan = new DeliveryMan(this.signInForm.value);
    this.deliverManService.getLoginDeliveryMan(deliverMan.email, deliverMan.password).subscribe(newdeliverMan => {
      if (newdeliverMan) {
        this.message = 0;
        (newdeliverMan.verified) ? this.update(newdeliverMan, true) : this.message = 4;
      } else {
        this.message = 1;
      }

    });
  }
  verified() {

    if (this.newdeliverman.token === this.code) {
      this.verif = false
      this.update(this.newdeliverman, true)
    }
    else {
      this.verif = true
    }
  }
  //permet d'enregistrer les coordonnées du livreur et faire le mise à jour après l'authentification 
  update(deliveryman: DeliveryMan, notsocial?) {
    let connection = new ConnectionInfo(true);
    deliveryman.connectionInfo = connection;

    if ('geolocation' in navigator) {

      navigator.geolocation.getCurrentPosition((position) => {
        deliveryman.coordinates = { latitude: position.coords.latitude, longitude: position.coords.longitude }

        this.deliverManService.updateDeliveryManLocation(deliveryman).subscribe(newdeliverMan => {
          this.loginService.login(newdeliverMan._id, notsocial);
        });
        this.deliverManService.updateDeliveryMan(deliveryman).subscribe(newdeliverMan => {
          this.delievryman = newdeliverMan
        });
      }, (error) => {

        this.googleService.getLocation().subscribe(coords => {
          deliveryman.coordinates = { latitude: coords.location.lat, longitude: coords.location.lng }
          this.deliverManService.updateDeliveryManLocation(deliveryman).subscribe(newdeliverMan => {
            this.loginService.login(newdeliverMan._id, notsocial);
          })
          this.deliverManService.updateDeliveryMan(deliveryman).subscribe(newdeliverMan => {
            this.delievryman = newdeliverMan
          });


        })
      });
    }
  }
  //permet de vérifier si l'email valide ou non lors de l'inscription 
  check() {
    if (this.getSignupFormEmail.valid) {
      this.deliveryFeeService.getOneDeliveryFee().subscribe(data => {
        let value = data as any
        this.deliverManService.checkMail(value.key, this.signUpForm.value.email).subscribe(data => {
          if (data.status === 200) {
            this.message = 0;
          }
          else {
            this.message = 3
          }
        })
      })
    }
  }
  /*   signup(){
      let deliverMan= new DeliveryMan(this.signUpForm.value);
      this.deliverManService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
        deliverMan.cinImage = data 
        deliverMan.active=false;
        this.deliverManService.createDeliveryMan(deliverMan).subscribe(newdeliverMan=>{
          this.message=0
          if(newdeliverMan===true as any ){
            this.message=2;
          }else if(newdeliverMan!=null) {
            this.message=6;
            this.new=false;
            
            this.signUpFormBuild();
            
          }
    
        }  );
      },err=>{this.message=5; } )
  
  
    } */
  //permet de faire l'inscription 
  signup() {
    delete this.signUpForm.value.passwordConfirmation;
    let deliverMan = new DeliveryMan(this.signUpForm.value);
    deliverMan.active = false;
    this.deliverManService.createDeliveryMan(deliverMan).subscribe(newdeliverMan => {
      this.newdeliverman = newdeliverMan
      this.message = 0
      if (this.newdeliverman === true as any) {
        this.message = 2;
      } else if (this.newdeliverman != null) {
        this.message = 6;
        //this.new=false;
        // ici nous devons envoyer  newdeliverMan.token par sms au numeéro 
        this.smsService.checkSms(newdeliverMan.phoneNumber, "le code d'activation de votre compte est  " + this.newdeliverman.token).subscribe(res => console.log(res))
        this.signUpFormBuild();
        this.Modal.show()

      }

    });


  }
  // permet l'inscription  par  google et facebook
  signupSocial(user: SocialUser) {
    let deliveryman = new DeliveryMan({ 'firstName': user.firstName, 'lastName': user.lastName, 'email': user.email, 'password': user.id, 'verified': true, 'active': false });
    this.delievryman = deliveryman;
    this.basicModal.show()

  }
  save() {
    this.delievryman.phoneNumber = this.phonenumber;
    this.deliverManService.createDeliveryManSocial(this.delievryman).subscribe(newdeliverMan => {
      if (newdeliverMan === true as any) {
        this.message = 2;
      } else if (newdeliverMan != null) {
        this.message = 0;
        // ici nous devons envoyer  newdeliverMan.token par sms au numeéro 
        this.deliverManService.confirmDeliveryManMailSocial(newdeliverMan._id).subscribe(data => console.log(data))
        this.loginService.login(newdeliverMan._id, false)

      }
    })


  }
  // permet l'authentifcation par  google et facebook
  signinSocial(user: SocialUser) {
    let deliveryman = new DeliveryMan({ 'email': user.email, 'password': user.id });
    this.deliverManService.getLoginDeliveryMan(deliveryman.email, deliveryman.password).subscribe(newdeliverMan => {
      if (newdeliverMan) {
        this.message = 0;
        (newdeliverMan.verified) ? this.update(newdeliverMan, false) : this.message = 4;

      } else {
        this.message = 1;
      }

    })
    this.deliverManService.updateDeliveryMan(deliveryman).subscribe(newdeliverMan => {
      newdeliverMan.isConnected == true
    })


  }
  // permet de vérifer 
  signWithSocial(social, action): void {

    let provider = (social === 'FB') ? FacebookLoginProvider.PROVIDER_ID : GoogleLoginProvider.PROVIDER_ID;
    this.authService.signIn(provider).then(user => {
      (action === 'IN') ? this.signinSocial(user) : this.signupSocial(user)
    });

  }


  //Switching method for the password hide and show
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleFieldTextType1(){
    this.fieldTextType1=!this.fieldTextType1;
  }

}


import { Component, OnInit, ViewChild } from '@angular/core';
import { Delivery } from 'src/app/_models/delivery';
import {DeliveryService} from '../../_services/delivery/delivery.service';
import { DeliveryManLoginService } from 'src/app/_services/deliveryMan/delivery-login.service';
import { OrderService } from 'src/app/_services/order/order.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { readmore } from './../../_services/truncate'

@Component({
  selector: 'app-affected-delivery-list',
  templateUrl: './affected-delivery-list.component.html',
  styleUrls: ['./affected-delivery-list.component.scss']
})
export class AffectedDeliveryListComponent implements OnInit {

  @ViewChild('framee', { static: true })  formModal;
  change = readmore;
  Delivery:Delivery[] = []
  selectedId="";
  constructor(private DeliveryService:DeliveryService,private loginService:DeliveryManLoginService,
    private orderService:OrderService,private recipientService:RecipientService,private senderService:SenderService) { }

  ngOnInit() {
    this.getDelivery();
  }
  getDelivery(){
    this.DeliveryService.getDeliveryByDelievryManAndStatus(this.loginService.getDeliveryMan(),"affected").subscribe(Delivery=>{
      this.Delivery = Delivery;
      this.Delivery.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index),this.getSender(index)})

    });
  }
  getSender(id){
    let delivery = this. Delivery[id];
    if(delivery.sender){
      this.senderService.getSender(delivery.sender).subscribe(sender=>{
        delivery.sender=  sender as any
      });
    }
  }
  getOrder(id){
    let delivery = this. Delivery[id];
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }
    getRecipient(id){
      let delivery = this. Delivery[id];
      if(delivery.recipient){
        this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
          delivery.recipient=  recipient.firstName + " "+ recipient.lastName;
        });
      }  
    }
  accept(){
    this.DeliveryService.getDelivery(this.selectedId).subscribe(delivery=>{
      delivery.status="accepted";
      this.DeliveryService.updateDelivery(delivery).subscribe(updatedDelivery=>{
        this.getDelivery()
      })
    }) 
  }

  refuse(){

    this.DeliveryService.getDelivery(this.selectedId).subscribe(delivery=>{
      delivery.status="wait";
      delivery.deliveryMan=null;
      this.DeliveryService.updateDelivery(delivery).subscribe(updatedDelivery=>{
        this.getDelivery()
      })
    }) 


  }
}

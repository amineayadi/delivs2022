import { Component, OnInit } from '@angular/core';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import { Product } from 'src/app/_models/product';
import { isUndefined } from 'util';
import { DeliveryManLoginService } from 'src/app/_services/deliveryMan/delivery-login.service';
@Component({
  selector: 'app-delivery-man-profil',
  templateUrl: './delivery-man-profil.component.html',
  styleUrls: ['./delivery-man-profil.component.scss']
})
export class DeliveryManProfilComponent implements OnInit {
  DeliveryMan:DeliveryMan = new DeliveryMan();
  selectedProduct : Product = new Product();
    passwordUpdate:Boolean=false;
    updateCIN:boolean=false;
    cinImage:any="";
    selectedFiles: FileList;
    social=false;
  constructor(private DeliveryManService:DeliveryManService,private loginService:DeliveryManLoginService) { }

  ngOnInit() {
    this.getDeliveryMan();
  }
  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.cinImage=this.selectedFiles.item(0).name
  }
  close(){
    this.cinImage="";
    this.updateCIN=false;
  }

  getDeliveryMan(){
    this.DeliveryManService.getDeliveryMan(this.loginService.getDeliveryMan()).subscribe(DeliveryMan=>{
      this.DeliveryMan = DeliveryMan;
      console.log(DeliveryMan)
    });
    this.social=this.loginService.isSocial() as any }
  
  updateImage(){
    this.DeliveryManService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
      this.DeliveryMan.cinImage = data 
      this.update();
      this.close();
    })
     }
  
  update(){
   this.DeliveryManService.updateDeliveryMan(this.DeliveryMan).subscribe(DeliveryMan=>{ this.DeliveryMan = DeliveryMan;} )

    
  }
  
}


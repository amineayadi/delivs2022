import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryManProfilComponent } from './delivery-man-profil.component';

describe('DeliveryManProfilComponent', () => {
  let component: DeliveryManProfilComponent;
  let fixture: ComponentFixture<DeliveryManProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryManProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryManProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

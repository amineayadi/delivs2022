import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriqueLivraisonComponent } from './historique-livraison.component';

describe('HistoriqueLivraisonComponent', () => {
  let component: HistoriqueLivraisonComponent;
  let fixture: ComponentFixture<HistoriqueLivraisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoriqueLivraisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriqueLivraisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

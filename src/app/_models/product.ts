export class Product {
    _id:string;
    id:string;
    name:string;
    description:string;
    price:any;
    available:boolean;
    dayAvailable:number;
    image:string;
    category:string;
    options : Option[];
    public constructor() {
        this.category="0"
       this.options=[];
       this.dayAvailable=7;
    }


}
export class Option {
    _id:string;
    optionName:string;
    comment:string;
    buttonType:boolean;
    optionLines : OptionLine[];
    public constructor() {
        this.optionLines=[]
     }
}
export class OptionLine {
    _id:string;
    name:string;
    price:number;
    checked:boolean
}

import { Product } from './product';

export class Sender {
    _id:string;
    name:string;
    lastName:string;
    phoneNumber:String;
    cin:string;
    address:string;
    rib:string;
    email:string;
    password:string;
    banner:any;
    type:string;
    menu:Product[];
    times:Times[];
    minOrder: number;
    verified:boolean;
    closed:boolean;
    active:boolean;
    token:string;

    delivsFee:DelivsFee = new DelivsFee();
    coordinates:{
		latitude:number,
		longitude:number
	}
    public constructor(init?: Partial<Sender >) {
        Object.assign(this, init);
    }



}
export class Times {
    _id:string;
    Day:number;
	DayofTheWeek:string;
	openningTime:string;
	closingTime:string;
	closed:boolean;
    public constructor(DayofTheWeek,day) {
        this.DayofTheWeek=DayofTheWeek;
        this.Day=day;
        this.closed=false;
    }


}


export class DelivsFee{
    typeFee:number;
	fee:number;
    method:number;
    public constructor() {
        this.typeFee=0;
        this.method=0;
        this.fee=0;
    }
}
export function getNewPrice(delivsfee:DelivsFee,price){
     if(delivsfee.method===0){
         if(delivsfee.typeFee===0){
             return price+delivsfee.fee
         }
         else{
            return price+(delivsfee.fee*price)/100;
         }
     }
     else{
        return price;
     }
}
export function getDelivsPrice(delivsfee:DelivsFee,price){
        if(delivsfee.typeFee===0){
            return delivsfee.fee
        }
        else{
           return(delivsfee.fee*price)/100;
        }
    
   
}
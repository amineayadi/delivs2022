import { ConnectionInfo } from './recipient';

export class DeliveryMan {

    _id:string;
    firstName:string;
    lastName:string;
    phoneNumber:string;
    cin:string;
    cinImage:any;
    address:string;
    rib:string;
    email:string;
    password:string;
    active:boolean;
    available:boolean;
    verified:boolean;
    isConnected:boolean;
    connectionInfo:ConnectionInfo;
    coordinates:{
		latitude:number,
		longitude:number
	}
    token:string;
    public constructor(init?: Partial<DeliveryMan >) {
        Object.assign(this, init);
    }


}
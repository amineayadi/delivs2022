export class OrderItem {
    id:string;
    product:string;
    quantity:number;
    price:number;
    delivs:number;
    comment:string;
    public constructor(product,quantity,price,delivs?,comment?) {
       this.product=product;
       this.quantity=quantity;
       this.price=price;
       this.comment=comment;
       this.delivs=delivs;
    }



}
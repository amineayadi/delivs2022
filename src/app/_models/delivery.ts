export class Delivery {

    _id:string;
    sender:string;
    deliveryMan:string;
    object:string;
    description:string;
    recipient:string;
    recipientNumber:string;
    startingPoint:string;
    startingcoordinates:{
        latitude:number,
        longitude:number
    };
    arrivalcoordinates:{
    latitude:number,
    longitude:number
    }
    arrivalPoint:string;
    deliveryDate:Date;
    distance:number;
    estimatedDuration:string;
    startTime:Date;
    arrivalTime:Date;
    status:string;
    price:number;
    deliveryFee:number;
    delivsFee:number;
    total:number;
    payment:string;
    reviewed:boolean
    statusHistory:history[]=[]


}
export class history{
    status:string;
    public constructor(status) {
        this.status=status;
    }
}